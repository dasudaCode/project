package charge.feign;

import com.zhiyuan.remote.ChargeRemote;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "chargeProvider4031")
public interface ChargeClient extends ChargeRemote {
}
