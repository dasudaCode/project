package charge.controller;

import charge.feign.ChargeClient;
import com.zhiyuan.pojo.ChargePro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class ChargeController {

    @Autowired
    private ChargeClient client;

    @RequestMapping("/consumer/charge/viewCharge")
    public Object Charge(@RequestParam  Map<String, Object> params) {
        return client.Charge(params);
    }

    @RequestMapping("/consumer/charge/addPro")
    public Object addpro(@RequestBody ChargePro pro) {
        return client.addpro(pro);
    }

    @RequestMapping("/consumer/charge/allChargePro")
    public Object allpro() {
        return client.allpro();
    }
    @RequestMapping("/consumer/charge/proByid")
    public Object pro(@RequestParam("proId") Integer proId) {
        return client.pro(proId);
    }

    @RequestMapping("/consumer/charge/updatePro")
    public Object update(@RequestBody ChargePro pro) {
        return client.update(pro);
    }
}
