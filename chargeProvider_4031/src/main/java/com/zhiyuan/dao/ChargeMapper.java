package com.zhiyuan.dao;


import com.zhiyuan.pojo.Charge;
import com.zhiyuan.pojo.ChargePro;
import com.zhiyuan.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface ChargeMapper {

    List<Charge> chargeInfo(Map<String, Object> params);

    Integer chargeCount(Map<String, Object> params);

    Integer addCpro(ChargePro pro);

    List<User> allUser(ChargePro pro);

    Integer addCharge(@Param("proId") Integer proId,@Param("userId") Integer userId);

    List<ChargePro> allCpro();

    ChargePro proByid(Integer proId);

    Integer updatePro(ChargePro pro);

    List<ChargePro> selectRepeat();
}
