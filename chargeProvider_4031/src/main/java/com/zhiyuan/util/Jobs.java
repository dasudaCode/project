package com.zhiyuan.util;

import com.zhiyuan.dao.ChargeMapper;
import com.zhiyuan.pojo.ChargePro;
import com.zhiyuan.pojo.User;
import com.zhiyuan.service.ChargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.List;

@Component
public class Jobs {
    @Autowired
    private ChargeMapper mapper;

    @Autowired
    private ChargeService service;
    //每天三点执行
    @Scheduled(cron="0 0 3 * * ? ")
    public void cronJob(){
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DATE);

        List<ChargePro> list = mapper.selectRepeat();
        for(ChargePro cp : list){
            if(cp.getRepeat() == day){
                List<User> list2 = service.alluser(cp);
                for(User u : list2){
                    Integer a = (Integer) service.addcharge(cp.getProId(),u.getUserId());
                }
            }
        }
    }
}
