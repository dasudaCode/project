package com.zhiyuan.service;


import com.zhiyuan.dao.ChargeMapper;
import com.zhiyuan.pojo.ChargePro;
import com.zhiyuan.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ChargeService {
    @Autowired
    private ChargeMapper mapper;



    //分页查询所有缴费过的用户
    public Object chargeUser(Map<String,Object> params){
        if(params.get("status") == null || params.get("status") =="" ){
            params.put("status",0);
        }
        if(params.get("plotId") == null || params.get("plotId").equals("0") || params.get("plotId") == ""){
            params.put("plotId",null);
        }
        if(params.get("proName") == null || params.get("proName") ==""){
            params.put("proName",null);
        }
        String  p = (String) params.get("page");
        String l = (String) params.get("limit");
        Integer page = Integer.valueOf(p);
        Integer limit = Integer.valueOf(l);
        params.put("start",(page-1)*limit);
        params.put("size",limit);
        return mapper.chargeInfo(params);
    }

    //分页数量
    public Integer chargeUserCount(Map<String,Object> params){
        if(params.get("status") == null || params.get("status") =="" ){
            params.put("status",0);
        }
        if(params.get("plotId") == null || params.get("plotId").equals("0") || params.get("plotId") == ""){
            params.put("plotId",null);
        }
        if(params.get("proName") == null || params.get("proName") ==""){
            params.put("proName",null);
        }
        return mapper.chargeCount(params);
    }

    //新增缴费方案
    public Integer addCpro(ChargePro pro){
        if(pro.getRepeat() == 0){
            pro.setRepeatTime(null);
        }
        return mapper.addCpro(pro);
    }


    //通过plotId查该小区的角色信息
    public List<User> alluser(ChargePro pro){
        if(pro.getPlotId() == 0){
            pro.setPlotId(null);
        }
        return mapper.allUser(pro);
    }

    //新增的缴费准确的加在每个角色记录上
    public Object addcharge(Integer proId,Integer userId){
        return mapper.addCharge(proId,userId);
    }

    //全部缴费方案
    public Object allpro(){
        return mapper.allCpro();
    }

    //通过ID查方案
    public Object proByid(Integer proId){
        return mapper.proByid(proId);
    }

    //修改方案
    public Integer updatePro(ChargePro pro){
        return  mapper.updatePro(pro);
    }
}
