package com.zhiyuan.controller;

import com.zhiyuan.pojo.ChargePro;
import com.zhiyuan.pojo.User;
import com.zhiyuan.remote.ChargeRemote;
import com.zhiyuan.service.ChargeService;
import com.zhiyuan.utils.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class ChargeController implements ChargeRemote {
    @Autowired
    private ChargeService service;
    //分页查询用户缴费信息
    @Override
    public Object Charge(Map<String, Object> params) {
        CommonResult result = new CommonResult();
        result.setCount(service.chargeUserCount(params));
        result.setData(service.chargeUser(params));
        result.setCode(0);
        return result;
    }

    //发布新的缴费方案，并给每一个用户加上缴费记录
    @Override
    public Object addpro(ChargePro pro) {
        service.addCpro(pro);
        Integer proId = pro.getProId();
        List<User> list = service.alluser(pro);
        for(User u : list){
            Integer a = (Integer) service.addcharge(proId,u.getUserId());
            if(a == 1){
                return 200;

            }
        }
       return 404;
    }

    //全部缴费方案
    @Override
    public Object allpro() {
        return service.allpro();
    }

    //通过ID查方案
    @Override
    public Object pro(Integer proId) {
        return service.proByid(proId);
    }

    //修改方案
    @Override
    public Object update(ChargePro pro) {
        if(service.updatePro(pro) == 1){
            return 200;
        }else{
            return 404;
        }
    }
}
