package com.zhiyuan.dao;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Mapper
@Repository
public interface CountMapper {

    Integer userCount(@Param("plotId") Integer plotId);

    Integer rootCount(@Param("plotId") Integer plotId);

    Integer serviceCount(@Param("plotId") Integer plotId);

    Integer complaintCount(@Param("plotId") Integer plotId);

    Integer serviceCountByMonth(Map<Object,Object> params);

    Integer complaintCountByMonth(Map<Object,Object> params);

    Integer chargeCountByMonth(Map<Object,Object> params);
}
