package com.zhiyuan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/11
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableTransactionManagement
public class CountProvider_4061 {
    public static void main(String[] args) {
        SpringApplication.run(CountProvider_4061.class,args);
    }
}
