package com.zhiyuan.controller;

import com.zhiyuan.remote.CountRemote;
import com.zhiyuan.service.CountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CountController implements CountRemote {
    @Autowired
    private CountService service;
    @Override
    public Object userCount(Integer plotId) {
        return service.usercount(plotId);
    }

    @Override
    public Object rootCount(Integer plotId) {
        return service.rootcount(plotId);
    }

    @Override
    public Object serviceCount(Integer plotId) {
        return service.servicecount(plotId);
    }

    @Override
    public Object complaintCount(Integer plotId) {
        return service.complaintcount(plotId);
    }

    @Override
    public Object serviceCountByMonth(Integer plotId, Integer year) {
        return service.serviceCountByMonth(plotId, year);
    }

    @Override
    public Object complaintCountByMonth(Integer plotId, Integer year) {
        return service.complaintCountByMonth(plotId, year);
    }

    @Override
    public Object chargeCountByMonth(Integer plotId, Integer year) {
        return service.chargeCountByMonth(plotId, year);
    }
}
