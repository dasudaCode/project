package com.zhiyuan.service;


import com.zhiyuan.dao.CountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CountService {
    @Autowired
    private CountMapper mapper;

    public Integer usercount(Integer plotId){
        return mapper.userCount(plotId);
    }

    public Integer rootcount(Integer plotId){
        return mapper.rootCount(plotId);
    }

    public Integer servicecount(Integer plotId){
        return mapper.serviceCount(plotId);
    }

    public Integer complaintcount(Integer plotId){
        return mapper.complaintCount(plotId);
    }

    public Object serviceCountByMonth(Integer plotId,Integer year){
        Map<Object,Object> params = new HashMap<>();
        params.put("plotId",plotId);
        params.put("year",year);
        Integer [] service = new Integer[12];
        for(int i=0;i<12;i++){
            int month = i+1;
            params.put("month",month);
            service[i] = mapper.serviceCountByMonth(params);
        }
        return service;
    }

    public Object chargeCountByMonth(Integer plotId,Integer year){
        Map<Object,Object> params = new HashMap<>();
        params.put("plotId",plotId);
        params.put("year",year);
        Integer [] service = new Integer[12];
        for(int i=0;i<12;i++){
            int month = i+1;
            params.put("month",month);
            service[i] = mapper.chargeCountByMonth(params);
        }
        return service;
    }

    public Object complaintCountByMonth(Integer plotId,Integer year){
        Map<Object,Object> params = new HashMap<>();
        params.put("plotId",plotId);
        params.put("year",year);
        Integer [] service = new Integer[12];
        for(int i=0;i<12;i++){
            int month = i+1;
            params.put("month",month);
            service[i] = mapper.complaintCountByMonth(params);
        }
        return service;
    }
}
