package com.zhiyuan.pojo;

import java.io.Serializable;

public class homeInfo implements Serializable {
    private String title = "首页";
    private String href = "page/welcome-1.html?t=1";

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
