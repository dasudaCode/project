package com.zhiyuan.pojo;

import java.io.Serializable;

public class Plot implements Serializable {
    private Integer plotId;

    private String plotName;

    private String plotAddress;

    public Integer getPlotId() {
        return plotId;
    }

    public void setPlotId(Integer plotId) {
        this.plotId = plotId;
    }

    public String getPlotName() {
        return plotName;
    }

    public void setPlotName(String plotName) {
        this.plotName = plotName == null ? null : plotName.trim();
    }

    public String getPlotAddress() {
        return plotAddress;
    }

    public void setPlotAddress(String plotAddress) {
        this.plotAddress = plotAddress == null ? null : plotAddress.trim();
    }
}