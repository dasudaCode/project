package com.zhiyuan.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class Repair implements Serializable {
    private Integer repairId;

    private Integer userId;

    private String userName;

    private Integer plotId;

    private String plotName;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date repairTime;

    private String repairInfo;

    private Integer repairDel = 0;

    private String repairIdentify;

    private String repairBefore;

    private String repairAfter;

    private String repairReply;

    private Integer repairMark;

    public String getRepairReply() {
        return repairReply;
    }

    public void setRepairReply(String repairReply) {
        this.repairReply = repairReply;
    }

    public Integer getRepairMark() {
        return repairMark;
    }

    public void setRepairMark(Integer repairMark) {
        this.repairMark = repairMark;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getPlotId() {
        return plotId;
    }

    public void setPlotId(Integer plotId) {
        this.plotId = plotId;
    }

    public String getPlotName() {
        return plotName;
    }

    public void setPlotName(String plotName) {
        this.plotName = plotName;
    }

    public Integer getRepairId() {
        return repairId;
    }

    public void setRepairId(Integer repairId) {
        this.repairId = repairId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getRepairTime() {
        return repairTime;
    }

    public void setRepairTime(Date repairTime) {
        this.repairTime = repairTime;
    }

    public String getRepairInfo() {
        return repairInfo;
    }

    public void setRepairInfo(String repairInfo) {
        this.repairInfo = repairInfo == null ? null : repairInfo.trim();
    }

    public Integer getRepairDel() {
        return repairDel;
    }

    public void setRepairDel(Integer repairDel) {
        this.repairDel = repairDel;
    }

    public String getRepairIdentify() {
        return repairIdentify;
    }

    public void setRepairIdentify(String repairIdentify) {
        this.repairIdentify = repairIdentify == null ? null : repairIdentify.trim();
    }

    public String getRepairBefore() {
        return repairBefore;
    }

    public void setRepairBefore(String repairBefore) {
        this.repairBefore = repairBefore == null ? null : repairBefore.trim();
    }

    public String getRepairAfter() {
        return repairAfter;
    }

    public void setRepairAfter(String repairAfter) {
        this.repairAfter = repairAfter == null ? null : repairAfter.trim();
    }
}