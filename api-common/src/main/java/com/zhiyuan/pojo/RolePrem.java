package com.zhiyuan.pojo;

import java.io.Serializable;

public class RolePrem implements Serializable {
    private Integer roleId;

    private Integer premId;

    private Integer rpId;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getPremId() {
        return premId;
    }

    public void setPremId(Integer premId) {
        this.premId = premId;
    }

    public Integer getRpId() {
        return rpId;
    }

    public void setRpId(Integer rpId) {
        this.rpId = rpId;
    }
}