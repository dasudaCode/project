package com.zhiyuan.pojo;

import java.io.Serializable;

public class ChargePro implements Serializable {
    private Integer proId;
    private Integer roleId;
    private String proName;
    private String proMoney;
    private String proInfo;
    private Integer repeat;
    private Integer repeatTime;
    private Integer proDel;
    private Integer plotId;

    public Integer getPlotId() {
        return plotId;
    }

    public void setPlotId(Integer plotId) {
        this.plotId = plotId;
    }

    public Integer getProDel() {
        return proDel;
    }

    public void setProDel(Integer proDel) {
        this.proDel = proDel;
    }

    public Integer getRepeat() {
        return repeat;
    }

    public void setRepeat(Integer repeat) {
        this.repeat = repeat;
    }

    public Integer getRepeatTime() {
        return repeatTime;
    }

    public void setRepeatTime(Integer repeatTime) {
        this.repeatTime = repeatTime;
    }

    public Integer getProId() {
        return proId;
    }

    public void setProId(Integer proId) {
        this.proId = proId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getProMoney() {
        return proMoney;
    }

    public void setProMoney(String proMoney) {
        this.proMoney = proMoney;
    }

    public String getProInfo() {
        return proInfo;
    }

    public void setProInfo(String proInfo) {
        this.proInfo = proInfo;
    }
}
