package com.zhiyuan.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class Complain implements Serializable {
    private Integer complainId;

    private Integer userId;

    private String userName;

    private Integer plotId;

    private String plotName;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date complainTime;

    private String complainInfo;

    private Integer complainDel=0;

    private String complainIdentify;

    private String complainReply;

    private Integer complainMark;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getPlotId() {
        return plotId;
    }

    public void setPlotId(Integer plotId) {
        this.plotId = plotId;
    }

    public String getPlotName() {
        return plotName;
    }

    public void setPlotName(String plotName) {
        this.plotName = plotName;
    }

    public String getComplainReply() {
        return complainReply;
    }

    public void setComplainReply(String complainReply) {
        this.complainReply = complainReply;
    }

    public Integer getComplainMark() {
        return complainMark;
    }

    public void setComplainMark(Integer complainMark) {
        this.complainMark = complainMark;
    }

    public Integer getComplainId() {
        return complainId;
    }

    public void setComplainId(Integer complainId) {
        this.complainId = complainId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getComplainTime() {
        return complainTime;
    }

    public void setComplainTime(Date complainTime) {
        this.complainTime = complainTime;
    }

    public String getComplainInfo() {
        return complainInfo;
    }

    public void setComplainInfo(String complainInfo) {
        this.complainInfo = complainInfo == null ? null : complainInfo.trim();
    }

    public Integer getComplainDel() {
        return complainDel;
    }

    public void setComplainDel(Integer complainDel) {
        this.complainDel = complainDel;
    }

    public String getComplainIdentify() {
        return complainIdentify;
    }

    public void setComplainIdentify(String complainIdentify) {
        this.complainIdentify = complainIdentify == null ? null : complainIdentify.trim();
    }
}