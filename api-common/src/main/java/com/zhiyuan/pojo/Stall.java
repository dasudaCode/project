package com.zhiyuan.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/18
 */
public class Stall implements Serializable {
    private Integer stallId;

    private String stallCode;

    private String stallState;

    private String stallRares;

    private Integer plotId;

    private String plotName;

    private String userName;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date stallTime;

    public Date getStallTime() {
        return stallTime;
    }

    public void setStallTime(Date stallTime) {
        this.stallTime = stallTime;
    }

    public String getPlotName() {
        return plotName;
    }

    public void setPlotName(String plotName) {
        this.plotName = plotName;
    }

    public Integer getStallId() {
        return stallId;
    }

    public void setStallId(Integer stallId) {
        this.stallId = stallId;
    }

    public String getStallCode() {
        return stallCode;
    }

    public void setStallCode(String stallCode) {
        this.stallCode = stallCode == null ? null : stallCode.trim();
    }

    public String getStallState() {
        return stallState;
    }

    public void setStallState(String stallState) {
        this.stallState = stallState == null ? null : stallState.trim();
    }

    public String getStallRares() {
        return stallRares;
    }

    public void setStallRares(String stallRares) {
        this.stallRares = stallRares == null ? null : stallRares.trim();
    }

    public Integer getPlotId() {
        return plotId;
    }

    public void setPlotId(Integer plotId) {
        this.plotId = plotId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }
}
