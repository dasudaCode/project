package com.zhiyuan.pojo;

import java.io.Serializable;

public class logoInfo implements Serializable {
    private String title = "致远物业";
    private String image = "images/logo.png";
    private String href = "" ;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
