package com.zhiyuan.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class Notice implements Serializable {
    private Integer noticeId;

    private String noticeName;

    private String noticeOb;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date noticeSt;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date noticeEnd;

    private String noticeInfo;

    private Integer noticeDel = 0;

    public Integer getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(Integer noticeId) {
        this.noticeId = noticeId;
    }

    public String getNoticeName() {
        return noticeName;
    }

    public void setNoticeName(String noticeName) {
        this.noticeName = noticeName == null ? null : noticeName.trim();
    }

    public String getNoticeOb() {
        return noticeOb;
    }

    public void setNoticeOb(String noticeOb) {
        this.noticeOb = noticeOb == null ? null : noticeOb.trim();
    }

    public Date getNoticeSt() {
        return noticeSt;
    }

    public void setNoticeSt(Date noticeSt) {
        this.noticeSt = noticeSt;
    }

    public Date getNoticeEnd() {
        return noticeEnd;
    }

    public void setNoticeEnd(Date noticeEnd) {
        this.noticeEnd = noticeEnd;
    }

    public String getNoticeInfo() {
        return noticeInfo;
    }

    public void setNoticeInfo(String noticeInfo) {
        this.noticeInfo = noticeInfo == null ? null : noticeInfo.trim();
    }

    public Integer getNoticeDel() {
        return noticeDel;
    }

    public void setNoticeDel(Integer noticeDel) {
        this.noticeDel = noticeDel;
    }
}