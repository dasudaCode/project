package com.zhiyuan.pojo;

import java.io.Serializable;
import java.util.Map;

public class User implements Serializable {
    private Integer userId;

    private Integer plotId;

    private String userName;

    private String userTel;

    private String userAddress;

    private String userInfo;

    private Integer userDel;

    private String userCode;

    private Integer userCheck;

    private String userPwd;

    private String plotName;

    private String roleName;

    private String oldroleName;

    private Integer roleId;

    private Map<String,Role> map;

    public Map<String, Role> getMap() {
        return map;
    }

    public void setMap(Map<String, Role> map) {
        this.map = map;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getOldroleName() {
        return oldroleName;
    }

    public void setOldroleName(String oldroleName) {
        this.oldroleName = oldroleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getPlotName() {
        return plotName;
    }

    public void setPlotName(String plotName) {
        this.plotName = plotName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPlotId() {
        return plotId;
    }

    public void setPlotId(Integer plotId) {
        this.plotId = plotId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getUserTel() {
        return userTel;
    }

    public void setUserTel(String userTel) {
        this.userTel = userTel == null ? null : userTel.trim();
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress == null ? null : userAddress.trim();
    }

    public String getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(String userInfo) {
        this.userInfo = userInfo == null ? null : userInfo.trim();
    }

    public Integer getUserDel() {
        return userDel;
    }

    public void setUserDel(Integer userDel) {
        this.userDel = userDel;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode == null ? null : userCode.trim();
    }

    public Integer getUserCheck() {
        return userCheck;
    }

    public void setUserCheck(Integer userCheck) {
        this.userCheck = userCheck;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd == null ? null : userPwd.trim();
    }
}