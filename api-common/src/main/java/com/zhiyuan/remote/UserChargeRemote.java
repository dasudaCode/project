package com.zhiyuan.remote;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@RequestMapping("/consumer/UserCharge")
public interface UserChargeRemote {
    //通过用户ID，年份，月份查询缴费
    @RequestMapping("/selectCharge")
    Object selectCharge(@RequestParam Map<String,Object> params);

}
