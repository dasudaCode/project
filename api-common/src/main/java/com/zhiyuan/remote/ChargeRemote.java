package com.zhiyuan.remote;

import com.zhiyuan.pojo.ChargePro;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@RequestMapping("/charge")
public interface ChargeRemote {

    //分页查询用户缴费信息
    @RequestMapping("/viewCharge")
    Object Charge(@RequestParam Map<String,Object> params);

    //新增缴费方案
    @RequestMapping("/addPro")
    Object addpro(@RequestBody ChargePro pro);

    //全部方案
    @RequestMapping("/allChargePro")
    Object allpro();

    //id查方案
    @RequestMapping("/proByid")
    Object pro(@RequestParam("proId") Integer proId);

    //修改方案
    @RequestMapping("/updatePro")
    Object update(@RequestBody ChargePro pro);
}
