package com.zhiyuan.remote;

import com.zhiyuan.pojo.Complain;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/20
 */
public interface ComplainRemote {

    @RequestMapping("/selectComplainByPage")
    CommonResult<List<Complain>> selectComplainByPage(@RequestBody Map<String,Object> params);

    @PostMapping("/addComplain")
    CommonResult<Complain> addComplain(@RequestBody Complain complain);

    @PostMapping("/updateComplain")
    CommonResult<Complain> updateComplain(@RequestBody Complain complain);

    @PostMapping("/delComplain")
    CommonResult<Complain> delComplain(@RequestParam("complainId") Integer complainId);


}
