package com.zhiyuan.remote;

import com.zhiyuan.pojo.Plot;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/12
 */
public interface PlotRemote {

    @PostMapping("/addPlot")
    CommonResult<String> addPlot(@RequestBody Plot plot);

    @PostMapping("/updatePlot")
    CommonResult<String> updatePlot(@RequestBody Plot plot);

    @PostMapping("/delPlot")
    CommonResult<String> delPlot(@RequestParam("plotId") Integer plotId);

    @RequestMapping("/selectPlotByPage")
    CommonResult<List<Plot>> selectPlotByPage(@RequestBody Map<String,Object> params);

    @GetMapping("/getAllPlot")
    List<Plot> getAllPlot();
}
