package com.zhiyuan.remote;

import com.zhiyuan.pojo.Repair;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/21
 */
public interface RepairRemote {

    @PostMapping("/delRepair")
    CommonResult<Repair> delRepair(@RequestParam("repairId") Integer repairId);

    /**
     * 添加报修数据
     *
     * @param repair
     * @return 添加结果
     */
    @PostMapping("/insertRepair")
    CommonResult<Repair> insertRepair(@RequestBody Repair repair);

    /**
     * 修改报修数据
     * @param repair
     * @return 修改结果
     */
    @PostMapping("/updateRepair")
    CommonResult<Repair> updateRepair(@RequestBody Repair repair);

    /**
     * 有条件的分页查询数据
     * @param params start 数据的下标，size数据的每页条数，repairIdentify 处理情况,plotId 小区Id
     * @return 报修集合
     */
    @RequestMapping("/selectRepairByPage")
    CommonResult<List<Repair>> selectRepairByPage(@RequestBody Map<String,Object> params);

}
