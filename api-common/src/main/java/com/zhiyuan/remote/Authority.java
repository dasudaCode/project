package com.zhiyuan.remote;

import com.zhiyuan.pojo.Role;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/consumer/admin")
public interface Authority {
    //根据登录的用户code返回对应权限的导航json
    @RequestMapping("/init")
    Object init(@RequestParam("userCode") String userCode);

    //权限管理——用户赋权的所有用户
    @RequestMapping("/allUser")
    Object user(@RequestParam(value = "plotId",required = false) Integer plotId, @RequestParam(value = "userName",required = false) String userName ,@RequestParam("page")Integer page,@RequestParam("limit") Integer limit);

    //用户赋予角色
    @RequestMapping("/userEmpower")
    Object empower(@RequestBody String[] roleIdsss);
    //用户ID查角色
    @RequestMapping("/rolebyId")
    Object rolebyId(@RequestParam("userId") Integer userId);
    //全部角色
    @RequestMapping("/allRole")
    Object allrole(@RequestParam("page")Integer page, @RequestParam("limit")Integer limit);

    //添加角色
    @RequestMapping("/addRole")
    Object addrole(@RequestBody Role role);

    //角色ID查权限
    @RequestMapping("/perbyId")
    Object perbyId(@RequestParam("roleId")Integer roleId);

    //通过ID删角色权限
    @RequestMapping("/delbyroleId")
    Object delbyroleId(@RequestParam("roleId")Integer roleId);

    //通过角色ID赋权限
    @RequestMapping("/roleaddper")
    Object roleaddper(@RequestBody Integer [] roleadss);

    //所有权限
    @RequestMapping("/allPermission")
    Object allPer();

    //所有角色不分页数据
    @RequestMapping("/allrole2")
    Object allrole2();

    //通过roleId删除角色
    @RequestMapping("/delrolebyid")
    Object delrolebyid(@RequestParam("roleId") Integer roleId);
}
