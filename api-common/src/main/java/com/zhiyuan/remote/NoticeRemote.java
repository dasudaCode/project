package com.zhiyuan.remote;

import com.zhiyuan.pojo.Notice;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/15
 */
public interface NoticeRemote {

    @PostMapping("/addNotice")
    CommonResult<Notice> addNotice(@RequestBody Notice notice);

    @PostMapping("/updateNotice")
    CommonResult<Notice> updateNotice(@RequestBody Notice notice);

    @PostMapping("/delNotice")
    CommonResult<Notice> delNotice(@RequestParam("noticeId") Integer noticeId);

    @RequestMapping("/selectNoticeByPage")
    CommonResult<List<Notice>> selectNoticeByPage(@RequestBody Map<String,Object> params);
}
