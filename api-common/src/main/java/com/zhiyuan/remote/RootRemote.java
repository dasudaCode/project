package com.zhiyuan.remote;

import com.zhiyuan.pojo.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;
@RequestMapping("/consumer/root")
public interface RootRemote {

    //所有管理员
    @RequestMapping("/viewRoot")
    Object rootuser(@RequestParam Map<String,Object> params);

    //管理员的类型
    @RequestMapping("/rootType")
    Object rootType();

    //新增管理员
    @RequestMapping("/addRoot")
    Object addroot(@RequestBody User user);

    //查看userCode是否重复
    @RequestMapping("/codeExist")
    Object codeexist(@RequestParam("userCode") String userCode);

    //修改root用户
    @RequestMapping("/uproot")
    Object updateRoot(@RequestBody User user);

    //删除root用户
    @RequestMapping("/delRoot")
    Object delroot(Integer userId);
}
