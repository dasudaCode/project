package com.zhiyuan.remote;

import com.zhiyuan.pojo.Note;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/17
 */
public interface NoteRemote {

    @RequestMapping("/selectNoteByPage")
    CommonResult<List<Note>> selectNoteByPage(@RequestBody Map<String,Object> params);

    @PostMapping("/addNote")
    CommonResult<Note> addNote(@RequestBody Note note);

    @PostMapping("/delNote")
    CommonResult<Note> delNote(@RequestParam("noteId") Integer noteId);
}
