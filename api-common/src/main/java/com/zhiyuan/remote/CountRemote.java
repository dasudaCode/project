package com.zhiyuan.remote;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@RequestMapping("/consumer/Count")
public interface CountRemote {
    @RequestMapping("/userCount")
    //总业主数
    Object userCount(@RequestParam("plotId") Integer plotId);

    @RequestMapping("/rootCount")
    //总管理员
    Object rootCount(@RequestParam("plotId") Integer plotId);

    @RequestMapping("/serviceCount")
    //总维修数量
    Object serviceCount(@RequestParam("plotId") Integer plotId);

    @RequestMapping("/complaintCount")
    //总投诉数量
    Object complaintCount(@RequestParam("plotId") Integer plotId);

    @RequestMapping("/serviceCountByMonth")
    //按月份查维修数量
    Object serviceCountByMonth(@RequestParam("plotId") Integer plotId, @RequestParam("year") Integer year);

    @RequestMapping("/complaintCountByMonth")
    //按月份查投诉数量
    Object complaintCountByMonth(@RequestParam("plotId") Integer plotId,@RequestParam("year") Integer year);

    @RequestMapping("/chargeCountByMonth")
    //按月份查缴费数量
    Object chargeCountByMonth(@RequestParam("plotId") Integer plotId,@RequestParam("year") Integer year);
}
