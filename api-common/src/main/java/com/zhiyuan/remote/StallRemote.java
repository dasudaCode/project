package com.zhiyuan.remote;

import com.zhiyuan.pojo.Stall;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/18
 */
public interface StallRemote {

    @RequestMapping("/selectStallByPage")
    CommonResult<List<Stall>> selectStallByPage(@RequestBody Map<String,Object> params);

    @PostMapping("/addStall")
    CommonResult<Stall> addStall(@RequestBody Stall stall);

    @PostMapping("/updateStall")
    CommonResult<Stall> updateStall(@RequestBody Stall stall);

    @PostMapping("/delStall")
    CommonResult<Stall> delStall(@RequestParam("stallId") Integer stallId);

    @RequestMapping("/selectStallById")
    CommonResult<Stall> selectStallById(@RequestParam("stallId") Integer stallId);
}
