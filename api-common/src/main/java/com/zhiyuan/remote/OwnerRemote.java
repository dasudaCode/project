package com.zhiyuan.remote;

import com.zhiyuan.pojo.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@RequestMapping("/consumer/owner")
public interface OwnerRemote {
    //分页查询未审核的业主
    @RequestMapping("/unowner")
    Object viewunOwner(@RequestParam Map<String,Object> params);

    //业主审核通过
    @RequestMapping("/pass")
    Integer pass(Integer userId);

    //删除未审核的业主
    @RequestMapping("/delUnowner")
    Integer delUnowner(Integer userId);

    //分页查询通过审核的业主
    @RequestMapping("/owner")
    Object viewOwner(@RequestParam Map<String,Object> params);

    //编辑通过审核的业主
    @RequestMapping("/upowner")
    Object upowner(@RequestBody User user);
    //删除通过审核审核的业主
    @RequestMapping("/delOwner")
    Integer delowner(Integer userId);

    //查用户信息
    @RequestMapping("/selectUser")
    Object selectUser(Integer userId);

    //修改用户信息
    @RequestMapping("/updateUser")
    Object updateUser(@RequestBody  User user);

    //修改密码
    @RequestMapping("/updatePwd")
    Object updatePwd(@RequestParam Map<String,Object> params );
}
