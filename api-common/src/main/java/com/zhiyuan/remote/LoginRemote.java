package com.zhiyuan.remote;

import com.zhiyuan.pojo.User;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/26
 */
public interface LoginRemote {

    @RequestMapping("/doLogin")
    CommonResult<User> doLogin(@RequestParam("userCode") String userCode,@RequestParam("userPwd") String userPwd);

    @RequestMapping("/addUser")
    CommonResult<User> addUser(@RequestBody User user);

    @RequestMapping("/sms")
    Object sms(@RequestParam("yzm") String yzm,@RequestParam("userTel") String userTel);

    @RequestMapping("/forgetPwd")
    Object forgetPwd(@RequestParam("userCode") String userCode,@RequestParam("yzm") String yzm);

    @RequestMapping("/newPwd")
    Object newPwd(@RequestParam("userCode") String userCode,@RequestParam("userPwd") String userPwd);
 }
