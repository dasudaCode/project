package com.zhiyuan.utils;

public class AlipayConfig {
    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2021000116662337";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCwLHqkwS7ez44wFbutv7gRLN5tKn465uHMYNxutHJpAhvOCwPV6Qbd8nT2gRnmQOg0G5TcwXXTI4SVlUBnGy712lJ0eW2WlGbm0oVuV7W1oGOTfJIru/KGI5Bi2IHnC3tcUiodKhiTWYsX3GFTv/X+IPSB7/vwQ/F+YuZTJi1Z/9z743H1c7BcpPKgvqVLlkGSyNGFkrRwGL469kF7hXCFAO57go31to/rzJQZa6oSzGGbHx6CN/fI7XfpB4im0L5yHJGUx/lND17ap+bP/D7XqmPzhiz1t/nLzcrWoXbVsJsxftsLaQpOfm09ejgAhPHYW/ISB2RX2DSikQ5oU7JlAgMBAAECggEAMJlkj9SW+HIrWEp2hpzPyzUketrxDZCpk3bY+FUhBqTFRNa3DSiGc22yJFpC6cjYBZ/iNe/McB1pMtaEeK0ADGRYwLbKq3Ju3JgQpPrfLqZ7MbF4pUXoxmZDK3FsjG8XbcVgxsDZHLTj8QYjdzYKRzuSF9I7HvGvSd1AiomKkzvT7mbyKSJICZ7rtmr2IefhWyqd56/qDmDc1TWGNvD30JPRdNzg8oO7elyFMW4n6tiOSRQeh7Kxn3afs1naXkrb9AaR/tUSr53v3NbZvobjLdj3l2g3LXhqSECr6LEsdbjGXWVYRKIMHX3o3gH0Fe+fIuK13cB1ftWOWngYvybdYQKBgQDWe9xYnclK2HrCiZ3fdJiSSNExHAzqhaoO10SDYzc4Zi/VTLokhowb/O41fARyxYg0B/fuqWwvl3k/u7ns1/j6YRRsCBNzGx8tJZuIRr0YhY3HkMf9szuiRx0qyiqfUXwmToESKU3dRyPXdVq6Rb16ddQ3riICBhBtod2ad06LjQKBgQDSRkVA9hFv3mEU7TDEElmNNHkBlqdArBKSIjorO/VBQpOAZmm9gt310naq4bh5zCmG4zj6PUlyuIJgj9XVIDtSZ72fNRFHjidtA0XLVjgPRZs5W+gE9C+uxwQyFy6m0ke9y2P/M5tpwO95IxcCB1xqR5XyxONQLR6E8hTXxDkgOQKBgQC0MGaThCuh13B6ksaw/fFXpBkq2tf3Bsgxo6QJhYbUuPSii80t9mMq+DjfcgsCqPSeUbt3I5qh+O2CH6I5Wc/v+TMA91/vqe9tLFUYDeW6/ih4aie5+aO112ynkCRyqDBWtkk1CsJBjgyMbCkGqKRghzD8fixP6uxOTlgv9t9j2QKBgAiiUAep0u93/x5/2sfCRSESLYHOMNHr/ts3QpIx5REn7ayRB/h7HigQ20SK/isqIe5i+Bz2VUS7pMbYQRx+NVFd5yBZans4sA++2afOw4CFCCmjx3QknqybiWWRJB4/diip6FI7ST9fZ7Xepfw0VywaAmM0iGzrXVxyID731KypAoGBAKMsdoOr51KeS/NF5KvkEoZSE9WW6prHZTdEjOk9cOME+3p5iQesrLOVFGOqlGULfJOYo4r2iJHyG0m6QT7TcgeRmsTMqSWa3MMy7gNzEQF7V4SlzLs+EPF9aqoSzSS531VfTKOPW5lGqnk3y1x89h+++H3Bwc+Bc4HrlHxpli7n";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key =
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoYrO5Vku6UJC4kShc09Q8wI1aWF+g7FLY7/S20AIiv0a2dVkJDZ1qxJozZQ1KFzbFgT1ohKvMVXFCey2o7f/d3r1Su8ybtFKYE5lvIbSePWt8uh8odR6GMg3FxVZ8S9jbKgZaGmHFIMRxXq0N2gLK+hwAIitHGUfSYvwPb0vedcSZHn6u8luEulf5eRRuj3aRYTOQhONwbJ5ibQi3PhMcmYpoY76/KbXmjkVnk2VYvCC89rxZuO6Q+d9A/53vewOQdn5oAv1mthyCzbjKGL1sYyHmxTfOnEfyI8OnKl3S6dJ+0URpeXsTNClSDFWhVyUkY3q4yJDPCDUFk3VVOkHnQIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://753d3c231f20.ngrok.io/UserCharge/notify_url";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://753d3c231f20.ngrok.io/UserCharge/return_url";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 支付宝网关
    public static String log_path = "C:\\";
}