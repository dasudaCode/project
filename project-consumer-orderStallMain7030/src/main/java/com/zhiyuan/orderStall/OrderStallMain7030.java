package com.zhiyuan.orderStall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/18
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class OrderStallMain7030 {
    public static void main(String[] args) {
        SpringApplication.run(OrderStallMain7030.class,args);
    }
}
