package com.zhiyuan.orderStall.controller;

import com.zhiyuan.orderStall.feign.StallFeign;
import com.zhiyuan.pojo.Stall;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/18
 */
@RestController
public class StallController {

    @Resource
    private StallFeign stallFeign;

    @RequestMapping("/consumer/stall/selectStallByPage")
    public CommonResult<List<Stall>> selectStallByPage(Integer page,Integer limit,String stallCode,Integer plotId) {
        Map<String,Object> params = new HashMap<>();
        if(page==null||page<=0){
            page = 1;
        }
        if(limit == null || limit <= 0){
            limit = 10;
        }
        params.put("start",(page-1)*limit);
        params.put("size",limit);
        if(stallCode!=null&&stallCode.length() > 0){
            params.put("stallCode",stallCode);
        }
        if(plotId!=null&&plotId>0){
            params.put("plotId",plotId);
        }
        return stallFeign.selectStallByPage(params);
    }

    @PostMapping("/consumer/stall/addStall")
    public CommonResult<Stall> addStall(Stall stall){
        return stallFeign.addStall(stall);
    }

    @PostMapping("/consumer/stall/updateStall")
    public CommonResult<Stall> updateStall(Stall stall){
        return stallFeign.updateStall(stall);
    }

    @PostMapping("/consumer/stall/delStall")
    public CommonResult<Stall> delStall(Integer stallId){
        return stallFeign.delStall(stallId);
    }

    @RequestMapping("/consumer/stall/reStallId")
    public CommonResult<Stall> reStallId(Integer stallId){
        return stallFeign.selectStallById(stallId);
    }
}
