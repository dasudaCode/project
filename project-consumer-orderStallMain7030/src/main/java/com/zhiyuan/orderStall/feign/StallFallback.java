package com.zhiyuan.orderStall.feign;

import com.zhiyuan.pojo.Stall;
import com.zhiyuan.remote.StallRemote;
import com.zhiyuan.utils.CommonResult;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/18
 */
@Component
public class StallFallback implements FallbackFactory<StallRemote> {
    @Override
    public StallRemote create(Throwable cause) {
        return new StallRemote() {
            @Override
            public CommonResult<List<Stall>> selectStallByPage(Map<String, Object> params) {
                return new CommonResult<>(500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<Stall> addStall(Stall stall) {
                return new CommonResult<>(500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<Stall> updateStall(Stall stall) {
                return new CommonResult<>(500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<Stall> delStall(Integer stallId) {
                return new CommonResult<>(500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<Stall> selectStallById(Integer stallId) {
                return new CommonResult<>(500,"服务器繁忙，请稍后再试！");
            }
        };
    }
}
