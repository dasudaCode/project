package com.zhiyuan.orderStall.feign;

import com.zhiyuan.remote.StallRemote;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/18
 */
@FeignClient(value = "project-stall-provider",fallbackFactory = StallFallback.class)
public interface StallFeign extends StallRemote {
}
