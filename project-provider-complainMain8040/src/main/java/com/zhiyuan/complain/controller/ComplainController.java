package com.zhiyuan.complain.controller;

import com.zhiyuan.complain.service.ComplainService;
import com.zhiyuan.pojo.Complain;
import com.zhiyuan.remote.ComplainRemote;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/20
 */
@RestController
public class ComplainController implements ComplainRemote {

    @Resource
    private ComplainService complainService;

    @Override
    public CommonResult<List<Complain>> selectComplainByPage(Map<String, Object> params) {
        int count = complainService.selectComplainCount(params);
        List<Complain> list = complainService.selectComplainByPage(params);
        return new CommonResult<>(0,"",count,list);
    }

    @Override
    public CommonResult<Complain> addComplain(Complain complain) {
        if(complainService.insertComplain(complain)){
            return new CommonResult<> (200,"添加投诉成功了！");
        }
        return new CommonResult<> (444,"添加投诉失败了！");
    }

    @Override
    public CommonResult<Complain> updateComplain(Complain complain) {
        if(complainService.updateComplain(complain)){
            return new CommonResult<> (200,"修改投诉成功了！");
        }
        return new CommonResult<> (444,"修改投诉失败了！");
    }

    @Override
    public CommonResult<Complain> delComplain(Integer complainId) {
        if(complainService.deleteComplainById(complainId)){
            return new CommonResult<> (200,"删除投诉成功了！");
        }
        return new CommonResult<> (444,"删除投诉失败了！");
    }
}
