package com.zhiyuan.complain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/19
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ComplainMain8040 {
    public static void main(String[] args) {
        SpringApplication.run(ComplainMain8040.class,args);
    }
}
