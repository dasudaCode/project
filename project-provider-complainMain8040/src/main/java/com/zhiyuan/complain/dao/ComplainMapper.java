package com.zhiyuan.complain.dao;

import com.zhiyuan.pojo.Complain;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/19
 */
@Mapper
public interface ComplainMapper {
    /**
     * 按主键ID删除投诉数据
     * @param complainId 主键
     * @return 删除结果
     */
    int deleteComplainById(@RequestParam("complainId")Integer complainId);

    /**
     * 添加投诉数据
     * @param complain
     * @return 添加结果
     */
    int insertComplain(Complain complain);

    /**
     * 修改投诉数据
     * @param complain
     * @return 修改结果
     */
    int updateComplain(Complain complain);

    /**
     * 分页查询投诉数据
     * @param params 条件查询的分页
     * @return 投诉对象的集合
     */
    List<Complain> selectComplainByPage(Map<String,Object> params);

    /**
     *
     * @param params 查询的条件 页数 当前页
     * @return 查询出来的总条数
     */
    int selectComplainCount(Map<String,Object> params);
}
