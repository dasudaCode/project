package com.zhiyuan.complain.service;

import com.zhiyuan.pojo.Complain;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/19
 */
public interface ComplainService {
    boolean deleteComplainById( Integer complainId);

    boolean insertComplain(Complain complain);

    boolean updateComplain(Complain complain);

    List<Complain> selectComplainByPage(Map<String,Object> params);

    int selectComplainCount(Map<String,Object> params);
}
