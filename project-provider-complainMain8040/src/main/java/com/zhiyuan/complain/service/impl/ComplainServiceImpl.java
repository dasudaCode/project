package com.zhiyuan.complain.service.impl;

import com.zhiyuan.complain.dao.ComplainMapper;
import com.zhiyuan.complain.service.ComplainService;
import com.zhiyuan.pojo.Complain;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/19
 */
@Service
public class ComplainServiceImpl implements ComplainService {

    @Resource
    private ComplainMapper complainMapper;
    @Override
    public boolean deleteComplainById(Integer complainId) {
        return complainMapper.deleteComplainById(complainId)>=1;
    }

    @Override
    public boolean insertComplain(Complain complain) {
        return complainMapper.insertComplain(complain) >= 1;
    }

    @Override
    public boolean updateComplain(Complain complain) {
        return complainMapper.updateComplain(complain) >= 1;
    }

    @Override
    public List<Complain> selectComplainByPage(Map<String, Object> params) {
        return complainMapper.selectComplainByPage(params);
    }

    @Override
    public int selectComplainCount(Map<String, Object> params) {
        return complainMapper.selectComplainCount(params);
    }
}
