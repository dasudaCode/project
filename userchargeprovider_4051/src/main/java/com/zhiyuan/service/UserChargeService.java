package com.zhiyuan.service;


import com.zhiyuan.dao.UserChargeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class UserChargeService {
    @Autowired
    private UserChargeMapper mapper;

    public Object selectCharge(Map<String, Object> params){
        return mapper.selectcharge(params);
    }

    public Object zfsuccess(String chargeId){
        return mapper.zfSuccess(chargeId);
    }
}
