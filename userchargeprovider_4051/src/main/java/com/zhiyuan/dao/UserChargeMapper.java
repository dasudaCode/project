package com.zhiyuan.dao;


import com.zhiyuan.pojo.Charge;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface UserChargeMapper {
    //查缴费记录
    List<Charge> selectcharge(Map<String, Object> params);

    //缴费成功
    Integer zfSuccess(String chargeId);
}
