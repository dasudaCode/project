package com.zhiyuan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/11
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableTransactionManagement
public class UserChargeProvider_4051 {
    public static void main(String[] args) {
        SpringApplication.run(UserChargeProvider_4051.class,args);
    }
}
