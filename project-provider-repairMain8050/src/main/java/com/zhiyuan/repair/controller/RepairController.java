package com.zhiyuan.repair.controller;

import com.zhiyuan.pojo.Repair;
import com.zhiyuan.remote.RepairRemote;
import com.zhiyuan.repair.service.RepairService;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/21
 */
@RestController
public class RepairController implements RepairRemote {

    @Resource
    private RepairService repairService;

    @Override
    public CommonResult<Repair> delRepair(Integer repairId) {
        if(repairService.deleteRepair(repairId)){
            return new CommonResult<> (200,"删除成功了！");
        }
        return new CommonResult<> (444,"删除失败了！");
    }

    @Override
    public CommonResult<Repair> insertRepair(Repair repair) {
        if(repairService.insertRepair(repair)){
            return new CommonResult<> (200,"添加成功了！");
        }
        return new CommonResult<> (444,"添加失败了！");
    }

    @Override
    public CommonResult<Repair> updateRepair(Repair repair) {
        if(repairService.updateRepair(repair)){
            return new CommonResult<> (200,"修改成功了！");
        }
        return new CommonResult<> (444,"修改失败了！");
    }

    @Override
    public CommonResult<List<Repair>> selectRepairByPage(Map<String, Object> params) {
        int count = repairService.selectRepairCount(params);
        List<Repair> list= repairService.selectRepairByPage(params);
        return new CommonResult<> (0,"",count,list);
    }
}
