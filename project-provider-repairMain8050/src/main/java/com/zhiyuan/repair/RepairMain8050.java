package com.zhiyuan.repair;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/21
 */
@SpringBootApplication
@EnableDiscoveryClient
public class RepairMain8050 {
    public static void main(String[] args) {
        SpringApplication.run(RepairMain8050.class,args);
    }
}
