package com.zhiyuan.repair.service;

import com.zhiyuan.pojo.Repair;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/21
 */
public interface RepairService {
    /**
     * 通过主键Id删除报修数据（逻辑删除）
     * @param repairId
     * @return 删除结果
     */
    boolean deleteRepair(@RequestParam("repairId") Integer repairId);

    /**
     * 添加报修数据
     *
     * @param repair
     * @return 添加结果
     */
    boolean insertRepair(Repair repair);

    /**
     * 修改报修数据
     * @param repair
     * @return 修改结果
     */
    boolean updateRepair(Repair repair);

    /**
     * 有条件的分页查询数据
     * @param params start 数据的下标，size数据的每页条数，repairIdentify 处理情况,plotId 小区Id
     * @return 报修集合
     */
    List<Repair> selectRepairByPage(Map<String,Object> params);

    /**
     * 分页查询总条数
     * @param params 查询条件
     * @return 数据总条数
     */
    int selectRepairCount(Map<String,Object> params);
}
