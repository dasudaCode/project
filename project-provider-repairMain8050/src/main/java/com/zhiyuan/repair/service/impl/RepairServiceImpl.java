package com.zhiyuan.repair.service.impl;

import com.zhiyuan.pojo.Repair;
import com.zhiyuan.repair.dao.RepairMapper;
import com.zhiyuan.repair.service.RepairService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/21
 */
@Service
public class RepairServiceImpl implements RepairService {

    @Resource
    private RepairMapper repairMapper;
    @Override
    public boolean deleteRepair(Integer repairId) {
        return repairMapper.deleteRepair(repairId)>=1;
    }

    @Override
    public boolean insertRepair(Repair repair) {
        return repairMapper.insertRepair(repair) >= 1;
    }

    @Override
    public boolean updateRepair(Repair repair) {
        return repairMapper.updateRepair(repair) >= 1;
    }

    @Override
    public List<Repair> selectRepairByPage(Map<String, Object> params) {
        return repairMapper.selectRepairByPage(params);
    }

    @Override
    public int selectRepairCount(Map<String, Object> params) {
        return repairMapper.selectRepairCount(params);
    }
}
