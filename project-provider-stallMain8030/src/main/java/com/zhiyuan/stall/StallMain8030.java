package com.zhiyuan.stall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/18
 */
@SpringBootApplication
@EnableDiscoveryClient
public class StallMain8030 {
    public static void main(String[] args) {
        SpringApplication.run(StallMain8030.class,args);
    }
}
