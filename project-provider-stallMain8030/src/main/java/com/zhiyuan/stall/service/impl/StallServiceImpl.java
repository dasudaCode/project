package com.zhiyuan.stall.service.impl;

import com.zhiyuan.pojo.Stall;
import com.zhiyuan.stall.dao.StallMapper;
import com.zhiyuan.stall.service.StallService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/18
 */
@Service
public class StallServiceImpl implements StallService {

    @Resource
    private StallMapper stallMapper;
    @Override
    public boolean deleteStallById(Integer stallId) {
        return stallMapper.deleteStallById(stallId)>=1;
    }

    @Override
    public boolean insertStall(Stall stall) {
        return stallMapper.insertStall(stall) >= 1;
    }

    @Override
    public boolean updateStall(Stall stall) {
        return stallMapper.updateStall(stall) >= 1;
    }

    @Override
    public List<Stall> selectStallByPage(Map<String, Object> params) {
        return stallMapper.selectStallByPage(params);
    }

    @Override
    public int selectStallCount(Map<String, Object> params) {
        return stallMapper.selectStallCount(params);
    }

    @Override
    public Stall selectStallById(Integer stallId) {
        return stallMapper.selectStallById(stallId);
    }
}
