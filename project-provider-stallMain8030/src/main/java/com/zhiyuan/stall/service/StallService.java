package com.zhiyuan.stall.service;

import com.zhiyuan.pojo.Stall;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/18
 */
public interface StallService {
    //删除车位
    boolean deleteStallById(Integer stallId);

    //添加车位
    boolean insertStall(Stall stall);

    //修改车位
    boolean updateStall(Stall stall);

    //分页查询
    List<Stall> selectStallByPage(Map<String,Object> params);

    int selectStallCount (Map<String,Object> params);

    Stall selectStallById(Integer stallId);
}
