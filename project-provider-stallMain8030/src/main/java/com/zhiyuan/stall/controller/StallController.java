package com.zhiyuan.stall.controller;

import com.zhiyuan.pojo.Stall;
import com.zhiyuan.remote.StallRemote;
import com.zhiyuan.stall.service.StallService;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/18
 */
@RestController
public class StallController implements StallRemote {

    @Resource
    private StallService stallService;
    @Override
    public CommonResult<List<Stall>> selectStallByPage(Map<String, Object> params) {
        int count = stallService.selectStallCount(params);
        List<Stall> list = stallService.selectStallByPage(params);
        return new CommonResult<>(0,"",count,list);
    }

    @Override
    public CommonResult<Stall> addStall(Stall stall) {
        if(stallService.insertStall(stall)){
            return new CommonResult<> (200,"success，添加成功了");
        }
        return new CommonResult<> (444,"fail，添加失败了");
    }

    @Override
    public CommonResult<Stall> updateStall(Stall stall) {
        if(stallService.updateStall(stall)){
            return new CommonResult<> (200,"success，修改成功了");
        }
        return new CommonResult<> (444,"fail，修改失败了");
    }

    @Override
    public CommonResult<Stall> delStall(Integer stallId) {
        if(stallService.deleteStallById(stallId)){
            return new CommonResult<> (200,"success，删除成功了");
        }
        return new CommonResult<> (444,"fail，删除失败了");
    }

    @Override
    public CommonResult<Stall> selectStallById(Integer stallId) {
        Stall stall = stallService.selectStallById(stallId);
        if(stall!=null){
            return new CommonResult<> (200,"",1,stall);
        }
        return new CommonResult<> (444,"没有存在的车位！");
    }
}
