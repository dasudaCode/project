package com.zhiyuan.stall.dao;

import com.zhiyuan.pojo.Stall;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/18
 */
@Mapper
public interface StallMapper {
    //删除车位
    int deleteStallById(@RequestParam("stallId") Integer stallId);

    //添加车位
    int insertStall(Stall stall);

    //修改车位
    int updateStall(Stall stall);

    //分页查询
    List<Stall> selectStallByPage(Map<String,Object> params);

    int selectStallCount (Map<String,Object> params);

    Stall selectStallById(@RequestParam("stallId") Integer stallId);
}
