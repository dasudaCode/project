package com.zhiyuan.note;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/15
 */
@SpringBootApplication
@EnableDiscoveryClient
public class NoteMain8020 {
    public static void main(String[] args) {
        SpringApplication.run(NoteMain8020.class,args);
    }
}
