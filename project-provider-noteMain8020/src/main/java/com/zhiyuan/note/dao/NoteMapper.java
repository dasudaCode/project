package com.zhiyuan.note.dao;

import com.zhiyuan.pojo.Note;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/17
 */
@Mapper
public interface NoteMapper {
    //删除留言
    int deleteNoteById(Integer noteById);

    //添加留言
    int insertNote(Note note);

    //查询留言分页
    List<Note> selectNoteByPage(Map<String,Object> params);

    //分页总数
    int selectNoteCount(Map<String,Object> params);
}
