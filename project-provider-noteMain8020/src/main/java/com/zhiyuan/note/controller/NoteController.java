package com.zhiyuan.note.controller;

import com.zhiyuan.note.service.NoteService;
import com.zhiyuan.pojo.Note;
import com.zhiyuan.remote.NoteRemote;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/17
 */
@RestController
public class NoteController implements NoteRemote {

    @Resource
    private NoteService noteService;
    @Override
    public CommonResult<List<Note>> selectNoteByPage(Map<String, Object> params) {
        int count = noteService.selectNoteCount(params);
        List<Note> list = noteService.selectNoteByPage(params);
        return new CommonResult<> (0,"",count,list);
    }

    @Override
    public CommonResult<Note> addNote(Note note) {
        if(noteService.insertNote(note)){
            return new CommonResult<>(200,"添加留言成功了！");
        }else{
            return new CommonResult<>(444,"添加留言失败了！");
        }
    }

    @Override
    public CommonResult<Note> delNote(Integer noteId) {
        if(noteService.deleteNoteById(noteId)){
            return new CommonResult<>(200,"删除留言成功了！");
        }else{
            return new CommonResult<>(444,"删除留言失败了！");
        }
    }
}
