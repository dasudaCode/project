package com.zhiyuan.note.service.impl;

import com.zhiyuan.note.dao.NoteMapper;
import com.zhiyuan.note.service.NoteService;
import com.zhiyuan.pojo.Note;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/17
 */
@Service
public class NoteServiceImpl implements NoteService {

    @Resource
    private NoteMapper noteMapper;
    @Override
    public boolean deleteNoteById(Integer noteById) {
        return noteMapper.deleteNoteById(noteById)>=1;
    }

    @Override
    public boolean insertNote(Note note) {
        return noteMapper.insertNote(note)>=1;
    }

    @Override
    public List<Note> selectNoteByPage(Map<String, Object> params) {
        return noteMapper.selectNoteByPage(params);
    }

    @Override
    public int selectNoteCount(Map<String, Object> params) {
        return noteMapper.selectNoteCount(params);
    }
}
