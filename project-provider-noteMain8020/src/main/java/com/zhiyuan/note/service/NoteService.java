package com.zhiyuan.note.service;

import com.zhiyuan.pojo.Note;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/17
 */
public interface NoteService {
    //删除留言
    boolean deleteNoteById(Integer noteById);

    //添加留言
    boolean insertNote(Note note);

    //查询留言分页
    List<Note> selectNoteByPage(Map<String,Object> params);

    //分页总数
    int selectNoteCount(Map<String,Object> params);
}
