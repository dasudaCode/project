package com.zhiyuan.login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/26
 */
@SpringBootApplication
@EnableDiscoveryClient
public class LoginMain8060 {
    public static void main(String[] args) {
        SpringApplication.run(LoginMain8060.class,args);
    }
}
