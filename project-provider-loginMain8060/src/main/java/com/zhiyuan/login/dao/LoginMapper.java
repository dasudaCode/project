package com.zhiyuan.login.dao;

import com.zhiyuan.pojo.Role;
import com.zhiyuan.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/26
 */
@Mapper
public interface LoginMapper {
    /**
     * 通过账号查找用户
     * @param userCode 用户账号
     * @return 用户对象
     */
    User selectUserByCode(@RequestParam("userCode") String userCode);

    /**
     * 通过用户Id查询角色Id
     * @param userId 用户ID
     * @return 角色Id的集合
     */
    List<Integer> selectRoleIdByUserId(@RequestParam("userId") Integer userId);

    /**
     * 通过角色Id查询角色
     * @param roleId 角色Id
     * @return 角色对象
     */
    Role selectRoleByid(@RequestParam("roleId") Integer roleId);

    /**
     * 添加用户
     * @param user 用户对象
     * @return
     */
    int insertUser(User user);

    /**
     * 给用户添加角色
     * @param userId
     * @return
     */
    int insertUserRole(Integer userId);

    /**
     * 验证用户是否存在
     * @param userCode
     * @return
     */
    User userCodeRepeat(String userCode);

    /**
     * 修改密码
     */

    Integer newPwd(@Param("userCode") String userCode, @Param("userPwd")String userPwd);
}
