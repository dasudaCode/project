package com.zhiyuan.login.controller;

import com.cloopen.rest.sdk.BodyType;
import com.cloopen.rest.sdk.CCPRestSmsSDK;
import com.zhiyuan.login.service.LoginService;
import com.zhiyuan.pojo.Role;
import com.zhiyuan.pojo.User;
import com.zhiyuan.remote.LoginRemote;
import com.zhiyuan.utils.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/26
 */
@RestController
public class LoginController implements LoginRemote {

    @Resource
    private LoginService loginService;

    @Autowired
    private RedisTemplate<Object,Object> template;

    @Override
    public CommonResult<User> doLogin(String userCode,String userPwd) {
        User user = loginService.selectUserByCode(userCode);
        if(user!=null){
            if(user.getUserCheck()==0){
                return new  CommonResult<>(444,"用户未审核");
            }
            if(user.getUserPwd().equals(userPwd)){
                List<Integer> rList= loginService.selectRoleIdByUserId(user.getUserId());
                Role role = loginService.selectRoleByid(rList.get(0));
                user.setRoleId(role.getRoleId());
                user.setRoleName(role.getRoleName());
                if(!template.hasKey(userCode)){
                    template.opsForValue().set(userCode,user,30,TimeUnit.MINUTES);
                    template.opsForValue().set(userCode+"time",1,25,TimeUnit.MINUTES);
                }
                return new CommonResult<>(200,"账号密码正确，登录成功",user);
            }else{
                return new CommonResult<>(444,"密码错误");
            }
        }else{
            return new CommonResult<>(404,"不存在的用户");
        }
    }

    @Override
    @Transactional
    public CommonResult<User> addUser(User user) {
        User u =loginService.codeRepeat(user.getUserCode());
        if(u != null){
            return new CommonResult(444,"用户账户重复");
        }else{
            boolean flag = loginService.insertUser(user);
            if(flag){
                return new CommonResult(200,"注册成功了");
            }
            return new CommonResult(444,"注册失败了");
        }
    }


    //短信调用
    public Object sms(String yzm,String userTel){
        //生产环境请求地址：app.cloopen.com
        String serverIp = "app.cloopen.com";
        //请求端口
        String serverPort = "8883";
        //主账号,登陆云通讯网站后,可在控制台首页看到开发者主账号ACCOUNT SID和主账号令牌AUTH TOKEN
        String accountSId = "8a216da873cec13201742ec49975242a";
        String accountToken = "b858005ca1ca4ae089c3300001ce0d95";
        //请使用管理控制台中已创建应用的APPID
        String appId = "8a216da873cec13201742ec49a862431";
        CCPRestSmsSDK sdk = new CCPRestSmsSDK();
        sdk.init(serverIp, serverPort);
        sdk.setAccount(accountSId, accountToken);
        sdk.setAppId(appId);
        sdk.setBodyType(BodyType.Type_JSON);
        String to = userTel;
        String templateId= "1";
        String[] datas = {yzm,"10","变量3"};
        String subAppend="1234";  //可选 扩展码，四位数字 0~9999
        String reqId="aaaa";  //可选 第三方自定义消息id，最大支持32位英文数字，同账号下同一自然天内不允许重复
        //HashMap<String, Object> result = sdk.sendTemplateSMS(to,templateId,datas);
        HashMap<String, Object> result = sdk.sendTemplateSMS(to,templateId,datas,subAppend,null);
        if("000000".equals(result.get("statusCode"))){
            //正常返回输出data包体信息（map）
            HashMap<String,Object> data = (HashMap<String, Object>) result.get("data");
            Set<String> keySet = data.keySet();
            for(String key:keySet){
                Object object = data.get(key);
                System.out.println(key +" = "+object);
            }
        }else{
            //异常返回输出错误码和错误信息
            System.out.println("错误码=" + result.get("statusCode") +" 错误信息= "+result.get("statusMsg"));
        }
        return new CommonResult(444,"添加失败了");
    }

    @Override
    public Object forgetPwd(String userCode, String yzm) {
        User u =loginService.codeRepeat(userCode);
        if(u == null){
            return new CommonResult(444,"用户不存在");
        }else{
            String tel = u.getUserTel();
            this.sms(yzm,u.getUserTel());
            return new CommonResult(200,"已向您的"+tel.substring(0,3)+"****"+tel.substring(7)+"发送验证短信");
        }
    }

    @Override
    public Object newPwd(String userCode,String userPwd) {
        if(loginService.newpwd(userCode, userPwd) == 1){
            return new CommonResult(200,"密码修改成功");
        }else{
            return new CommonResult(444,"密码修改失败");
        }
    }
}
