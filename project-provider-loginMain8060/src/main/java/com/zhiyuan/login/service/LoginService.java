package com.zhiyuan.login.service;

import com.zhiyuan.pojo.Role;
import com.zhiyuan.pojo.User;

import java.util.List;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/26
 */
public interface LoginService {
    /**
     * 通过账号查找用户
     * @param userCode 用户账号
     * @return 用户对象
     */
    User selectUserByCode(String userCode);

    /**
     * 通过用户Id查询角色Id
     * @param userId 用户ID
     * @return 角色Id的集合
     */
    List<Integer> selectRoleIdByUserId(Integer userId);

    /**
     * 通过角色Id查询角色
     * @param roleId 角色Id
     * @return 角色对象
     */
    Role selectRoleByid(Integer roleId);

    /**
     * 添加用户
     * @param user 用户对象
     * @return
     */
    boolean insertUser(User user);

    /**
     * 给用户添加角色
     * @param userId
     * @return
     */
    boolean insertUserRole(Integer userId);

    User codeRepeat(String userCode);

    /**
     * 修改密码
     */
    Integer newpwd(String userCode,String userPwd);
}
