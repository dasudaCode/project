package com.zhiyuan.login.service.impl;

import com.zhiyuan.login.dao.LoginMapper;
import com.zhiyuan.login.service.LoginService;
import com.zhiyuan.pojo.Role;
import com.zhiyuan.pojo.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/26
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Resource
    private LoginMapper loginMapper;
    @Override
    public User selectUserByCode(String userCode) {
        return loginMapper.selectUserByCode(userCode);
    }

    @Override
    public List<Integer> selectRoleIdByUserId(Integer userId) {
        return loginMapper.selectRoleIdByUserId(userId);
    }

    @Override
    public Role selectRoleByid(Integer roleId) {
        return loginMapper.selectRoleByid(roleId);
    }

    @Override
    public boolean insertUser(User user) {
        return loginMapper.insertUser(user) > 0;
    }

    @Override
    public boolean insertUserRole(Integer userId) {
        return loginMapper.insertUserRole(userId) > 0;
    }

    @Override
    public User codeRepeat(String userCode) {
        return loginMapper.userCodeRepeat(userCode);
    }

    @Override
    public Integer newpwd(String userCode, String userPwd) {
        return loginMapper.newPwd(userCode, userPwd);
    }
}
