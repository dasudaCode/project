package com.zhiyuan.orderNote.controller;

import com.zhiyuan.orderNote.feign.NoteFeign;
import com.zhiyuan.pojo.Note;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/17
 */
@RestController
public class NoteController {

    @Resource
    private NoteFeign noteFeign;

    @RequestMapping("/consumer/note/selectNoteByPage")
    public CommonResult<List<Note>> selectNoteByPage(Integer page,Integer limit,Integer noticeId){
        Map<String,Object> params = new HashMap<>();
        if(page==null||page<=0){
            page = 1;
        }
        if(limit == null || limit <= 0){
            limit = 10;
        }
        params.put("start",(page-1)*limit);
        params.put("size",limit);
        if(noticeId!=null){
            params.put("noticeId",noticeId);
        }
        return noteFeign.selectNoteByPage(params);
    }

    @PostMapping("/consumer/note/addNote")
    public CommonResult<Note> addNote(Note note){
        return noteFeign.addNote(note);
    }

    @PostMapping("/consumer/note/delNote")
    public CommonResult<Note> delNote(Integer noteId){
        return noteFeign.delNote(noteId);
    }
}
