package com.zhiyuan.orderNote.feign;

import com.zhiyuan.pojo.Note;
import com.zhiyuan.remote.NoteRemote;
import com.zhiyuan.utils.CommonResult;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/17
 */
@Component
public class NoteFallback implements FallbackFactory<NoteRemote> {
    @Override
    public NoteRemote create(Throwable cause) {
        return new NoteRemote() {
            @Override
            public CommonResult<List<Note>> selectNoteByPage(Map<String, Object> params) {
                return new CommonResult<> (500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<Note> addNote(Note note) {
                return new CommonResult<> (500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<Note> delNote(Integer noteId) {
                return new CommonResult<> (500,"服务器繁忙，请稍后再试！");
            }
        };
    }
}
