package com.zhiyuan.orderNote.feign;

import com.zhiyuan.remote.NoteRemote;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/17
 */
@FeignClient(value = "project-note-provider",fallbackFactory = NoteFallback.class)
public interface NoteFeign extends NoteRemote {
}
