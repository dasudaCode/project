package com.zhiyuan.orderNote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/17
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class OrderNoteMain7020 {
    public static void main(String[] args) {
        SpringApplication.run(OrderNoteMain7020.class,args);
    }
}
