package com.zhiyuan.service;


import com.zhiyuan.dao.OwnerMapper;
import com.zhiyuan.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class OwnerService {
    @Autowired
     private OwnerMapper mapper;
    //未审核业主
    public Object unowner(Map<String,Object> params){
        if(params.get("userName") == null || params.get("userName") =="" ){
            params.put("userName",null);
        }
        if(params.get("plotId") == null || params.get("plotId").equals("0") || params.get("plotId") == ""){
            params.put("plotId",null);
        }
        String  p = (String) params.get("page");
        String l = (String) params.get("limit");
        Integer page = Integer.valueOf(p);
        Integer limit = Integer.valueOf(l);
        params.put("start",(page-1)*limit);
        params.put("size",limit);
        return mapper.unowner(params);
    }

    //未审核业主总数
    public Integer unownerCount(Map<String,Object> params){
        if(params.get("userName") == null || params.get("userName") =="" ){
            params.put("userName",null);
        }
        if(params.get("plotId") == null || params.get("plotId").equals("0") || params.get("plotId") == ""){
            params.put("plotId",null);
        }
        return mapper.unownerCount(params);
    }

    //业主通过审核
    public Integer pass(Integer userId){
        return mapper.passed(userId);
    }

    //通过审核赋予角色
    public Integer userRole(Integer userId){
        return mapper.userrole(userId);
    }

    //删除用户
    public Integer delowner(Integer userId){
        return mapper.delowner(userId);
    }

    //通过审核业主
    public Object owner(Map<String,Object> params){
        if(params.get("userName") == null || params.get("userName") =="" ){
            params.put("userName",null);
        }
        if(params.get("plotId") == null || params.get("plotId").equals("0") || params.get("plotId") == ""){
            params.put("plotId",null);
        }
        String  p = (String) params.get("page");
        String l = (String) params.get("limit");
        Integer page = Integer.valueOf(p);
        Integer limit = Integer.valueOf(l);
        params.put("start",(page-1)*limit);
        params.put("size",limit);
        return mapper.owner(params);
    }

    //通过审核业主总数
    public Integer ownerCount(Map<String,Object> params){
        if(params.get("userName") == null || params.get("userName") =="" ){
            params.put("userName",null);
        }
        if(params.get("plotId") == null || params.get("plotId").equals("0") || params.get("plotId") == ""){
            params.put("plotId",null);
        }
        return mapper.ownerCount(params);
    }

    //修改通过审核的业主信息
    public Integer updateOwner(User user){
        return mapper.upowner(user);
    }

    public Integer updateuser(User user){
        return mapper.updateUser(user);
    }

    public User selectuser(Integer user){
        return mapper.selectUser(user);
    }

    public User selectPwd(Map<String,Object> params){
        return mapper.selectPwd(params);
    }

    public Integer updatepwd(Map<String,Object> params){
        return mapper.updatepwd(params);
    }
}
