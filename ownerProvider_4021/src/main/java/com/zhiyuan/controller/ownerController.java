package com.zhiyuan.controller;

import com.zhiyuan.pojo.User;
import com.zhiyuan.remote.OwnerRemote;
import com.zhiyuan.service.OwnerService;
import com.zhiyuan.utils.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@CrossOrigin
public class ownerController implements OwnerRemote {
    @Autowired
    private OwnerService service;


    @Override
    public Object viewunOwner(Map<String, Object> params) {
        CommonResult result = new CommonResult();
        result.setCount(service.unownerCount(params));
        result.setData(service.unowner(params));
        result.setCode(0);
        return result;
    }

    @Override
    public Integer pass(Integer userId) {
        service.pass(userId);
        if(service.userRole(userId)== 1){
            return 200;
        }else{
            return 404;
        }
    }

    @Override
    public Integer delUnowner(Integer userId) {
        if(service.delowner(userId) == 1){
            return 200;
        }else{
            return 404;
        }
    }

    @Override
    public Object viewOwner(Map<String, Object> params) {
        CommonResult result = new CommonResult();
        result.setCount(service.ownerCount(params));
        result.setData(service.owner(params));
        result.setCode(0);
        return result;
    }

    @Override
    public Object upowner(User user) {
        if(service.updateOwner(user) == 1){
            return 200;
        }else{
            return 404;
        }
    }

    @Override
    public Integer delowner(Integer userId) {

        if(service.delowner(userId) == 1){
            return 200;

        }else{
            return 404;
        }
    }

    @Override
    public Object selectUser(Integer userId) {
        return service.selectuser(userId);
    }

    @Override
    public Object updateUser(User user) {
        if(service.updateuser(user) ==1 ){
            return 200;
        }else{
            return 404;
        }
    }

    @Override
    public Object updatePwd(Map<String,Object> params) {
        if(service.selectPwd(params) == null){
            return "原密码错误";
        }else {
            if(service.updatepwd(params)==1){
                return 200;
            }else {
                return 404;
            }
        }
    }
}
