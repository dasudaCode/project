package com.zhiyuan.dao;


import com.zhiyuan.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
@Repository
public interface OwnerMapper {
    //未审核用户
    List<User> unowner(Map<String, Object> params);

    //未审核用户总数
    Integer unownerCount(Map<String, Object> params);

    //用户通过审核
    Integer passed(Integer userId);

    //通过审核赋予角色
    Integer userrole(Integer userId);

    //删除用户
    Integer delowner(Integer userId);

    //通过审核的用户
    List<User> owner(Map<String, Object> params);

    //通过审核用户总数
    Integer ownerCount(Map<String, Object> params);

    Integer upowner(User user);

    //用户修改个人资料
    Integer updateUser(User user);

    //查用户所有信息
    User selectUser(Integer userId);

    //查询密码是否正确
    User selectPwd(Map<String,Object> params);

    //修改密码
    Integer updatepwd(Map<String,Object> params);
}
