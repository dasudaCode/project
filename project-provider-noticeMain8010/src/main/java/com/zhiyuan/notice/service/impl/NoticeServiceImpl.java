package com.zhiyuan.notice.service.impl;

import com.zhiyuan.notice.dao.NoticeMapper;
import com.zhiyuan.notice.service.NoticeService;
import com.zhiyuan.pojo.Notice;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/15
 */
@Service
public class NoticeServiceImpl implements NoticeService {

    @Resource
    private NoticeMapper noticeMapper;
    @Override
    public boolean deleteNoticeById(Integer noticeId) {
        return noticeMapper.deleteNoticeById(noticeId)>=1;
    }

    @Override
    public boolean insertNotice(Notice notice) {
        return noticeMapper.insertNotice(notice)>=1;
    }

    @Override
    public boolean updateNotice(Notice notice) {
        return noticeMapper.updateNotice(notice) >= 1;
    }

    @Override
    public List<Notice> selectNoticeByPage(Map<String, Object> params) {
        return noticeMapper.selectNoticeByPage(params);
    }

    @Override
    public int selectNoticeCount(Map<String, Object> params) {
        return noticeMapper.selectNoticeCount(params);
    }
}
