package com.zhiyuan.notice.service;

import com.zhiyuan.pojo.Notice;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/15
 */
public interface NoticeService {
    //删除公告方法
    boolean deleteNoticeById(@RequestParam("noticeId") Integer noticeId);

    //添加公告方法
    boolean insertNotice(Notice notice);

    //修改公告方法
    boolean updateNotice(Notice notice);

    //查询公告分页
    List<Notice> selectNoticeByPage(Map<String,Object> params);

    //查询公告条数
    int selectNoticeCount(Map<String,Object> params);
}
