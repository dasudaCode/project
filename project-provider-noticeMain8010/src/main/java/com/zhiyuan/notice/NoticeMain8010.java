package com.zhiyuan.notice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/15
 */
@SpringBootApplication
@EnableDiscoveryClient
public class NoticeMain8010 {
    public static void main(String[] args) {
        SpringApplication.run(NoticeMain8010.class,args);
    }
}
