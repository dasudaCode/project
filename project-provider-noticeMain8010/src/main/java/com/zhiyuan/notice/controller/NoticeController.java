package com.zhiyuan.notice.controller;

import com.zhiyuan.notice.service.NoticeService;
import com.zhiyuan.pojo.Notice;
import com.zhiyuan.remote.NoticeRemote;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/15
 */
@RestController
public class NoticeController implements NoticeRemote {

    @Resource
    private NoticeService noticeService;
    @Override
    public CommonResult<Notice> addNotice(Notice notice) {
        if(noticeService.insertNotice(notice)){
            return new CommonResult<>(200,"success,添加成功了");
        }else{
            return new CommonResult<>(444,"fail,添加失败了");
        }
    }

    @Override
    public CommonResult<Notice> updateNotice(Notice notice) {
        if(noticeService.updateNotice(notice)){
            return new CommonResult<>(200,"success,修改成功了");
        }else{
            return new CommonResult<>(444,"fail,修改失败了");
        }
    }

    @Override
    public CommonResult<Notice> delNotice(Integer noticeId) {
        if(noticeService.deleteNoticeById(noticeId)){
            return new CommonResult<>(200,"success,删除成功了");
        }else{
            return new CommonResult<>(444,"fail,删除失败了");
        }
    }

    @Override
    public CommonResult<List<Notice>> selectNoticeByPage(Map<String, Object> params) {
        int count = noticeService.selectNoticeCount(params);
        List<Notice> list = noticeService.selectNoticeByPage(params);
        return new CommonResult<>(0,"",count,list);
    }
}
