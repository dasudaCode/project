package com.zhiyuan.gateway.filter;

import com.alibaba.fastjson.JSONObject;
import com.zhiyuan.pojo.User;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.RequestPath;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/28
 */
@Component
public class LoginFilter implements GlobalFilter, Ordered {

    @Resource
    private RedisTemplate<Object,Object> redisTemplate;
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //ServerHttpRequest request = exchange.getRequest();
        //MultiValueMap<String, HttpCookie> cookies = request.getCookies();
        //HttpCookie httpCookie = request.getCookies().getFirst("userCode");
        //RequestPath path = request.getPath();
        //String userCode = null;
        //JSONObject message = new JSONObject();
        //if (path.toString().contains("/doLogin")) {
        //    //5.说明该请求时登录
        //    return chain.filter(exchange); //继续向下执行
        //}
        ////判断cookie有没有userCode，有并且得到
        //if(httpCookie!=null){
        //    userCode = httpCookie.getValue();
        //}
        ////判断cookie的用户有没有登录或者过期
        //if(userCode!=null&&redisTemplate.hasKey(userCode)){
        //    //如果用户登录时间超过25分钟，刷新时间
        //    if(redisTemplate.hasKey(userCode+"time")){
        //        User user = (User) redisTemplate.opsForValue().get(userCode);
        //        redisTemplate.opsForValue().set(userCode,user,30, TimeUnit.MINUTES);
        //        redisTemplate.opsForValue().set(userCode+"time",1,25,TimeUnit.MINUTES);
        //    }
        //    return chain.filter(exchange);
        //}else{
        //    message.put("status",-1);
        //    message.put("msg","请登录！");
        //    byte[] bytes = message.toJSONString().getBytes(StandardCharsets.UTF_8);
        //    DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
        //    exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
        //    exchange.getResponse().getHeaders().add("Content-Type", "text/plain;charset=UTF-8");
        //    return exchange.getResponse().writeWith(Mono.just(buffer));
        //    //请求结束
        //}
        return  chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
