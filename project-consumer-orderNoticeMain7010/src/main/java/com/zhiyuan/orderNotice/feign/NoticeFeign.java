package com.zhiyuan.orderNotice.feign;

import com.zhiyuan.remote.NoticeRemote;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/15
 */
@FeignClient(value = "project-notice-provider",fallbackFactory = NoticeFallback.class)
public interface NoticeFeign extends NoticeRemote {

}
