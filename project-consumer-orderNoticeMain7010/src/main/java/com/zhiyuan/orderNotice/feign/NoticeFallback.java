package com.zhiyuan.orderNotice.feign;

import com.zhiyuan.pojo.Notice;
import com.zhiyuan.remote.NoticeRemote;
import com.zhiyuan.utils.CommonResult;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/15
 */
@Component
public class NoticeFallback implements FallbackFactory<NoticeRemote> {
    @Override
    public NoticeRemote create(Throwable cause) {
        return new NoticeRemote() {
            @Override
            public CommonResult<Notice> addNotice(Notice notice) {
                return new CommonResult<> (500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<Notice> updateNotice(Notice notice) {
                return new CommonResult<> (500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<Notice> delNotice(Integer noticeId) {
                return new CommonResult<> (500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<List<Notice>> selectNoticeByPage(Map<String, Object> params) {
                return new CommonResult<> (500,"服务器繁忙，请稍后再试！");
            }
        };
    }
}
