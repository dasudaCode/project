package com.zhiyuan.orderNotice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/15
 */
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class OrderNoticeMain7010 {
    public static void main(String[] args) {
        SpringApplication.run(OrderNoticeMain7010.class,args);
    }
}
