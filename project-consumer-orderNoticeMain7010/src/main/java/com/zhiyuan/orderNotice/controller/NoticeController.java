package com.zhiyuan.orderNotice.controller;

import com.zhiyuan.orderNotice.feign.NoticeFeign;
import com.zhiyuan.pojo.Notice;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/15
 */
@RestController
public class NoticeController {

    @Resource
    private NoticeFeign noticeFeign;

    @RequestMapping("/consumer/notice/selectNoticeByPage")
    public CommonResult<List<Notice>> selectNoticeByPage(Integer page,Integer limit,String noticeName) {
        Map<String,Object> params = new HashMap<>();
        if(page==null||page<=0){
            page = 1;
        }
        if(limit == null || limit <= 0){
            limit = 10;
        }
        params.put("start",(page-1)*limit);
        params.put("size",limit);
        if(noticeName!=null&&noticeName.length()>0){
            params.put("noticeName",noticeName);
        }
        return noticeFeign.selectNoticeByPage(params);
    }

    @PostMapping("/consumer/notice/addNotice")
    public CommonResult<Notice> addNotice(Notice notice){
        return noticeFeign.addNotice(notice);
    }

    @PostMapping("/consumer/notice/delNotice/{noticeId}")
    public CommonResult<Notice> delNotice(@PathVariable Integer noticeId){
        return noticeFeign.delNotice(noticeId);
    }

    @PostMapping("/consumer/notice/updateNotice")
    public CommonResult<Notice> updateNotice(Notice notice){
        return noticeFeign.updateNotice(notice);
    }
}
