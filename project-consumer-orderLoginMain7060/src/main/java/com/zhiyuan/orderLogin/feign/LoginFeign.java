package com.zhiyuan.orderLogin.feign;

import com.zhiyuan.remote.LoginRemote;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/26
 */
@FeignClient(value = "project-login-provider",fallbackFactory = LoginFallback.class)
public interface LoginFeign extends LoginRemote {
}
