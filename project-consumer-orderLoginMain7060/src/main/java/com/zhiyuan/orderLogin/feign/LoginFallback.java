package com.zhiyuan.orderLogin.feign;

import com.zhiyuan.pojo.User;
import com.zhiyuan.remote.LoginRemote;
import com.zhiyuan.utils.CommonResult;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/26
 */
@Component
public class LoginFallback implements FallbackFactory<LoginRemote> {
    @Override
    public LoginRemote create(Throwable cause) {
        return new LoginRemote() {
            @Override
            public CommonResult<User> doLogin(String userCode, String userPwd) {
                return new CommonResult<> (500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<User> addUser(User user) {
                return new CommonResult<> (500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public Object sms(String yzm, String userTel) {
                return null;
            }

            @Override
            public Object forgetPwd(String userCode, String yzm) {
                return new CommonResult<> (500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public Object newPwd(String userCode, String userPwd) {
                return new CommonResult<> (500,"服务器繁忙，请稍后再试！");
            }
        };
    }
}
