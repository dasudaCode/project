package com.zhiyuan.orderLogin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/26
 */
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class OrderLoginMain7060 {
    public static void main(String[] args) {
        SpringApplication.run(OrderLoginMain7060.class,args);
    }
}
