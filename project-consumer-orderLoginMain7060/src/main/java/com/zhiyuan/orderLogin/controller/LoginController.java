package com.zhiyuan.orderLogin.controller;

import com.zhiyuan.orderLogin.feign.LoginFeign;
import com.zhiyuan.pojo.User;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/26
 */
@RestController
public class LoginController {

    @Resource
    private LoginFeign loginFeign;

    @RequestMapping("/consumer/login/doLogin")
    public CommonResult<User> doLogin(String userCode,String userPwd){
        return loginFeign.doLogin(userCode,userPwd);
    }

    @RequestMapping("/consumer/login/addUser")
    public CommonResult<User> addUser(User user){
        return loginFeign.addUser(user);
    }

    @RequestMapping("/consumer/login/forgetPwd")
    public Object forgetPwd(@RequestParam("userCode") String userCode,@RequestParam("yzm") String yzm){
        return loginFeign.forgetPwd(userCode, yzm);
    }

    @RequestMapping("/consumer/login/newPwd")
    public Object newpwd(@RequestParam("userCode")String userCode ,@RequestParam("userPwd")String userPwd){
        return loginFeign.newPwd(userCode, userPwd);
    }

    @RequestMapping("/consumer/login/sms")
    public Object sms(@RequestParam("yzm") String yzm,@RequestParam("userTel") String userTel){
        return loginFeign.sms(yzm, userTel);
    }
}
