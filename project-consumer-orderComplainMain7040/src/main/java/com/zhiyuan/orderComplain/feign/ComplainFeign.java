package com.zhiyuan.orderComplain.feign;

import com.zhiyuan.remote.ComplainRemote;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/20
 */
@FeignClient(value = "project-complain-provider",fallbackFactory = ComplainFallback.class)
public interface ComplainFeign extends ComplainRemote {
}
