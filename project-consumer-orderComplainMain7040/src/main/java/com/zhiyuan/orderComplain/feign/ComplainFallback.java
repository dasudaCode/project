package com.zhiyuan.orderComplain.feign;

import com.zhiyuan.pojo.Complain;
import com.zhiyuan.remote.ComplainRemote;
import com.zhiyuan.utils.CommonResult;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/20
 */
@Component
public class ComplainFallback implements FallbackFactory<ComplainRemote> {
    @Override
    public ComplainRemote create(Throwable cause) {
        return new ComplainFeign() {
            @Override
            public CommonResult<List<Complain>> selectComplainByPage(Map<String, Object> params) {
                return new CommonResult<> (500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<Complain> addComplain(Complain complain) {
                return new CommonResult<> (500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<Complain> updateComplain(Complain complain) {
                return new CommonResult<> (500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<Complain> delComplain(Integer ComplainId) {
                return new CommonResult<> (500,"服务器繁忙，请稍后再试！");
            }
        };
    }
}
