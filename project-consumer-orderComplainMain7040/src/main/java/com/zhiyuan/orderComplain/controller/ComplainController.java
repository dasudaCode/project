package com.zhiyuan.orderComplain.controller;

import com.zhiyuan.orderComplain.feign.ComplainFeign;
import com.zhiyuan.pojo.Complain;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/20
 */
@RestController
public class ComplainController {

    @Resource
    private ComplainFeign complainFeign;

    @RequestMapping("/consumer/complain/selectComplainByPage")
    public CommonResult<List<Complain>> selectComplainByPage(Integer page,Integer limit,String complainIdentify,Integer plotId,Integer userId){
        Map<String,Object> params = new HashMap<>();
        if(page==null||page<=0){
            page = 1;
        }
        if(limit == null || limit <= 0){
            limit = 10;
        }
        params.put("start",(page-1)*limit);
        params.put("size",limit);
        if(complainIdentify!=null&&complainIdentify.length() > 0){
            params.put("complainIdentify",complainIdentify);
        }
        if(plotId != null && plotId>0){
            params.put("plotId",plotId);
        }
        if(userId != null && userId>0){
            params.put("userId",userId);
        }
        return complainFeign.selectComplainByPage(params);
    }

    @PostMapping("/consumer/complain/addComplain")
    public CommonResult<Complain> addComplain(Complain complain){
        complain.setComplainIdentify("未处理");
        return complainFeign.addComplain(complain);
    }

    @PostMapping("/consumer/complain/updateComplain")
    public CommonResult<Complain> updateComplain(Complain complain) {
        return complainFeign.updateComplain(complain);
    }

    @PostMapping("/consumer/complain/delComplain")
    public CommonResult delComplain(Integer complainId){
        return complainFeign.delComplain(complainId);
    }
}
