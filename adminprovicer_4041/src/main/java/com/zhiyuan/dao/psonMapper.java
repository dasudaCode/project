package com.zhiyuan.dao;

import com.zhiyuan.pojo.Permission;
import com.zhiyuan.pojo.Role;
import com.zhiyuan.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface psonMapper {
    //通过用户code查权限
    List<Permission> permisson(String userCode);

    //用户赋权的数据
    List<User> allUser(Map<String,Object> params);

    //查询用户的数据总量
    Integer userCount(Map<String,Object> params);

    //通过用户ID删除所有角色信息
    Integer delRole(Integer userId);

    //用户赋权
    Integer empower(@Param("roleId") Integer roleId, @Param("userId")Integer userId);

    //查询角色信息
    List<Role> allRole(@Param("start") Integer start, @Param("size")Integer size);

    //角色总数量
    Integer roleCount();

    //通过用户ID查权限
    List<User> userRole(Integer userId);

    //添加角色
    Integer addRole(Role role);

    //角色ID查权限
    List<Permission> perbyId(Integer roleId);

    //所有权限
    List<Permission> allPer();

    //通过角色ID删除权限
    Integer delbyroleId(Integer roleId);

    //通过角色ID添加权限
    Integer perbyroleid(@Param("roleId") Integer roleId,@Param("perId") Integer perId);

    //通过角色Id删除角色
    Integer delrolebyroleid(@Param("roleId") Integer roleId);
}
