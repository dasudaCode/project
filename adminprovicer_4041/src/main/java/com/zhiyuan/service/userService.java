package com.zhiyuan.service;

import com.zhiyuan.dao.psonMapper;
import com.zhiyuan.pojo.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.Map;

@Component
public class userService {
    @Autowired
    private psonMapper psonMapper;

    public Object alluser(Map<String,Object> params){
        return psonMapper.allUser(params);
    }

    public Integer userCount(Map<String,Object> params){
        return psonMapper.userCount(params);
    }

    public Integer empower(String [] role ){
        Integer userId =0;
        Integer roleId =0;
        for (int i=0;i<role.length;i++){
            userId = Integer.valueOf(role[0]);
            if(i == 0){
                psonMapper.delRole(Integer.valueOf(role[i]));
            }else{
                roleId = Integer.valueOf(role[i]);
                psonMapper.empower(roleId,userId);
            }

        }
            return 1;
    }

    public Object allRole(Integer page, Integer limit){
        Integer start = 0;
        Integer size = limit;
        if(page == null){
            start = 0;
        }else{
            start = (page-1)*limit;
        }
        return psonMapper.allRole(start,size);
    }
    public Integer roleCount(){
        return psonMapper.roleCount();
    }

    public Object userRole(Integer userId){
        return psonMapper.userRole(userId);
    }

    public Integer addrole(Role role){
        return psonMapper.addRole(role);
    }

    public Object perbyId(Integer userId){
        return psonMapper.perbyId(userId);
    }

    public Object delRoleId(Integer roleId){
        return psonMapper.delbyroleId(roleId);
    }

    public  Object allPer(){
        return psonMapper.allPer();
    }

    public Integer rolebyper(Integer roleId,Integer perId){
        return psonMapper.perbyroleid(roleId,perId);
    }

    public Integer delrolebyroleid(Integer roleId){
        return psonMapper.delrolebyroleid(roleId);
    }
}
