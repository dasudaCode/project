package com.zhiyuan.controller;

import com.zhiyuan.dao.psonMapper;
import com.zhiyuan.pojo.Permission;
import com.zhiyuan.pojo.Role;
import com.zhiyuan.pojo.homeInfo;
import com.zhiyuan.pojo.logoInfo;
import com.zhiyuan.remote.Authority;
import com.zhiyuan.service.userService;
import com.zhiyuan.utils.CommonResult;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class adminController implements Authority {
    @Autowired
    private psonMapper psonMapper;
    @Autowired
    private userService service;
    //通过用户登录的code查询权限
    @Override
    public Object init(String userCode) {
        Map<String,Object> map = new HashMap<>();
        homeInfo h = new homeInfo();
        logoInfo l = new logoInfo();
        Permission p = new Permission();
        List<Permission> list = psonMapper.permisson(userCode);
        p.setTitle("常规管理");
        p.setIcon("fa fa-address-book");
        p.setTarget("_self");
        p.setChild(list);
        List<Object> menulist = new ArrayList<>();
        menulist.add(p);
        map.put("logoInfo",l);
        map.put("homeInfo", h);
        map.put("menuInfo",menulist);
        return map;
    }
    //用户赋权的分页页面
    @Override
    public Object user(Integer plotId, String userName, Integer page, Integer limit) {
        Map<String,Object>  params = new HashMap<>();
        CommonResult<Object> commonResult = new CommonResult<>();
        if(plotId != null && plotId != 0){
            params.put("plotId",plotId);
        }else{
            params.put("plotId",null);
        }
        if(userName != null && userName != ""){
            params.put("userName",userName);
        }else{
            params.put("userName",null);
        }
        if(page == null){
            params.put("start",0);
            params.put("size",limit);
        }else{
            params.put("start",(page-1)*limit);
            params.put("size",limit);
        }
        commonResult.setCode(0);
        commonResult.setCount(service.userCount(params));
        commonResult.setData(service.alluser(params));
        return commonResult;
    }
    //点击赋权的操作
    @Override
    public Object empower(String[] roleIdsss) {
        if (!StringUtils.isEmpty(String.valueOf(roleIdsss))){
            service.empower(roleIdsss);
            return "200";
        }
        else{
           return "404";
        }
    }
    //用户赋权页面的查看操作
    public  Object rolebyId(Integer userId){
        return service.userRole(userId);
    }

    //全部角色
    @Override
    public Object allrole(Integer page, Integer limit){
        CommonResult<Object> commonResult = new CommonResult<>();
        commonResult.setCode(0);
        commonResult.setCount(service.roleCount());
        commonResult.setData(service.allRole(page, limit));
        return commonResult;
    }
    //添加角色
    @Override
    public Object addrole(Role role) {
        if(service.addrole(role) ==1){
            return "200";
        }else{
            return "404";
        }

    }

    //通过角色ID查权限
    public Object perbyId(Integer roleId){
        return service.perbyId(roleId);
    }

    //通过角色ID删权限
    @RequestMapping("/test8")
    @Override
    public Object delbyroleId(Integer roleId) {
        return service.delRoleId(roleId);
    }

    //通过角色ID赋权限
    @Override
    public Object roleaddper(Integer [] roleadss) {
        if (!StringUtils.isEmpty(String.valueOf(roleadss))){
            Integer roleId  = roleadss[0];
            service.delRoleId(roleId);
            for (int i = 1; i < roleadss.length; i++) {
                Integer idsss = roleadss[i];
                service.rolebyper(roleId,idsss);
            }
            return "200";
        }
        else{
            return "404";
        }
    }

    //所有权限
    @Override
    public Object allPer() {
        return service.allPer();
    }

    //全部角色信息（不分页）
    public Object allrole2(){
        return service.allRole(1, 10);
    }

    //通过角色ID删除角色
    public Object delrolebyid(Integer roleId){
        Integer i = service.delrolebyroleid(roleId);
        Integer j = (Integer) service.delRoleId(roleId);
        if(i==1 && j == 1){
            return "200";
        }else{
            return "404";
        }
    }
}
