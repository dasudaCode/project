package com.zhiyuan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/11
 */
@SpringBootApplication
@EnableDiscoveryClient
public class admin4041 {
    public static void main(String[] args) {
        SpringApplication.run(admin4041.class,args);
    }
}
