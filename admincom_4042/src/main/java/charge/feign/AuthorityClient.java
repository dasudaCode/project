package charge.feign;

import com.zhiyuan.remote.Authority;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "adminprovider_4041")
public interface AuthorityClient extends Authority {
}
