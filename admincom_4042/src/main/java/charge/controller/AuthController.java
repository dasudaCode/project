package charge.controller;

import charge.feign.AuthorityClient;
import com.zhiyuan.pojo.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

    @Autowired
    private AuthorityClient client;

    @RequestMapping("/nav")
    public Object init(String userCode) {
        return client.init(userCode);
    }

    @RequestMapping("/allUser")
    public Object user(Integer plotId, String userName, Integer page, Integer limit) {
        return client.user(plotId, userName, page, limit);
    }

    @RequestMapping("/giveAuth")
    public Object empower(String[] roleIdsss) {
        return client.empower(roleIdsss);
    }

    @RequestMapping("/viewUser")
    public Object rolebyId(Integer userId) {
        return client.rolebyId(userId);
    }

    @RequestMapping("/roleAll")
    public Object allrole(Integer page, Integer limit) {
        return client.allrole(page, limit);
    }

    @RequestMapping("/newRole")
    public Object addrole(Role role) {
        return client.addrole(role);
    }

    @RequestMapping("/perByroleid")
    public Object perbyId(Integer roleId) {
        return client.perbyId(roleId);
    }

    @RequestMapping("/delPer")
    public Object delbyroleId(Integer roleId) {
        return client.delbyroleId(roleId);
    }

    @RequestMapping("/newPer")
    public Object roleaddper(Integer[] roleadss) {
        return client.roleaddper(roleadss);
    }

    @RequestMapping("/perAll")
    public Object allPer() {
        return client.allPer();
    }

    @RequestMapping("roleAll2")
    public Object allrole2() {
        return client.allrole2();
    }

    @RequestMapping("/delRole")
    public Object delrolebyid(Integer roleId) {
        return client.delrolebyid(roleId);
    }
}
