package com.zhiyuan.controller;

import com.zhiyuan.pojo.User;
import com.zhiyuan.remote.RootRemote;
import com.zhiyuan.service.rootService;
import com.zhiyuan.utils.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
@RestController
@CrossOrigin
public class rootController implements RootRemote {
    @Autowired
    private rootService service;

    //分页查询管理员数据
    @Override
    public Object rootuser(Map<String, Object> params) {
        CommonResult com = new CommonResult();
        com.setCount(service.rootuserCount(params));
        com.setData(service.rootuser(params));
        com.setCode(0);
        return com;
    }

    //管理员类型
    @Override
    public Object rootType() {
        return service.roottype();
    }

    //新增管理员
    @Override
    public Object addroot(User user) {
        return service.addroot(user);
    }

    //查Code是否重复
    @Override
    public Object codeexist(String userCode) {
        return service.userCode(userCode);
    }

    //修改root用户
    @Override
    public Object updateRoot(User user) {
        Integer i = (Integer) service.updateroot(user);
        if(i == 1){
            return 200;
        }else{
            return 404;
        }
    }

    //删除root用户
    @Override
    public Object delroot(Integer userId) {
        Integer i = (Integer) service.delRoot(userId);
        if(i == 1){
            return 200;
        }else{
            return  404;
        }
    }
}
