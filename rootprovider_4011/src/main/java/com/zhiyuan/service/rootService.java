package com.zhiyuan.service;

import com.zhiyuan.dao.RootMapper;
import com.zhiyuan.pojo.Role;
import com.zhiyuan.pojo.User;
import com.zhiyuan.pojo.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Map;

@Service
public class rootService {
    @Autowired
    private RootMapper mapper;

    //所有管理的分页查询
    public Object rootuser(Map<String, Object> params){
        if(params.get("userCode") == null || params.get("userCode") =="" ){
           params.put("userCode",null);
        }
        if(params.get("roleName") == null || params.get("roleName").equals("0") || params.get("roleName") == ""){
            params.put("roleName",null);
        }
        String  p = (String) params.get("page");
        String l = (String) params.get("limit");
        Integer page = Integer.valueOf(p);
        Integer limit = Integer.valueOf(l);
        params.put("start",(page-1)*limit);
        params.put("size",limit);
        return mapper.rootuser(params);
    }

    //分页查询的总数量
    public  Integer rootuserCount(Map<String, Object> params){
        if(params.get("userCode") == null || params.get("userCode") =="" ){
            params.put("userCode",null);
        }
        if(params.get("roleName") == null || params.get("roleName").equals("0") || params.get("roleName") == ""){
            params.put("roleName",null);
        }
        return mapper.rootuserCount(params);
    }

    //管理员类型
    public  Object roottype(){
        return mapper.roleType();
    }

    //新增管理员
    public Integer addroot(User user){
        mapper.addRoot(user);
        Role role = mapper.roleNamebyid(user.getRoleName());
        Integer roleId = role.getRoleId();
        Integer userId = user.getUserId();
        Integer i  = mapper.addrole(userId,roleId);
        if(i == 1){
            return 200;
        }else{
            return 404;
        }
    }

    //查看userCode是否存在
    public Object userCode(String userCode){
        User u =  mapper.userCode(userCode);
        if (u.getUserId() == null){
            return "200";
        }else {
            return "404";
        }
    }

    //修改root
    @Transactional
    public Object updateroot(User user){
        try{
            Integer a = mapper.updateRoot(user);
            if(user.getOldroleName().equals(user.getRoleName())){
                return a;
            }else {
                //原来的role
                Role oldr = mapper.roleNamebyid(user.getOldroleName()); //2
                //需要改为的role
                Role newr = mapper.roleNamebyid(user.getRoleName()); //1

                UserRole ur = mapper.urid(user.getUserId(), oldr.getRoleId()); //2.2
                mapper.updateRootrole(ur.getUserId(), newr.getRoleId());
                return 1;
            }
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return 0;
        }
    }

    //删除用户
    @Transactional
    public  Object delRoot(Integer userId){
        try{
            mapper.delroot(userId);
            mapper.delrootur(userId);
            return 1;
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return 0;
        }
    }

}
