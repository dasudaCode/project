package com.zhiyuan.dao;

import com.zhiyuan.pojo.Role;
import com.zhiyuan.pojo.User;
import com.zhiyuan.pojo.UserRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface RootMapper {

    List<User> rootuser(Map<String, Object> params);

    Integer rootuserCount(Map<String, Object> params);

    List<Role> roleType();

    Integer addRoot(User user);

    Role roleNamebyid(String roleName);

    Integer addrole(@Param("userId") Integer userId, @Param("roleId") Integer roleId);

    User userCode(String userCode);

    Integer updateRoot(User user);

    UserRole urid(@Param("userId") Integer userId, @Param("roleId") Integer roleId);

    Integer updateRootrole(@Param("userId") Integer userId,@Param("roleId") Integer roleId);

    Integer delroot(Integer userId);

    Integer delrootur(Integer userId);

}
