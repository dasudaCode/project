package Root.feign;

import com.zhiyuan.remote.RootRemote;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "rootprovider_4011")
public interface RootClient extends RootRemote {
}
