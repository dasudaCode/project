package Root.Controller;

import Root.feign.RootClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class RootController {
    @Autowired
    private RootClient client;

    @RequestMapping("/rootUser")
    public Object rootuser(Map<String,Object> params){
        return client.rootuser(params);
    }
}
