package com.zhiyuan.orderRepair.feign;

import com.zhiyuan.pojo.Repair;
import com.zhiyuan.remote.RepairRemote;
import com.zhiyuan.utils.CommonResult;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/21
 */
@Component
public class RepairFallback implements FallbackFactory<RepairRemote> {
    @Override
    public RepairRemote create(Throwable cause) {
        return new RepairRemote() {
            @Override
            public CommonResult<Repair> delRepair(Integer repairId) {
                return new CommonResult<Repair> (500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<Repair> insertRepair(Repair repair) {
                return new CommonResult<>(500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<Repair> updateRepair(Repair repair) {
                return new CommonResult<>(500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<List<Repair>> selectRepairByPage(Map<String, Object> params) {
                return new CommonResult<>(500,"服务器繁忙，请稍后再试！");
            }
        };
    }
}
