package com.zhiyuan.orderRepair.feign;

import com.zhiyuan.remote.RepairRemote;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/21
 */
@FeignClient(value = "project-repair-provider",fallbackFactory = RepairFallback.class)
public interface RepairFeign extends RepairRemote {
}
