package com.zhiyuan.orderRepair.config;

import com.jcraft.jsch.*;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.Properties;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/21
 */
@Component
public class Sftp {
    private ChannelSftp sftp;

    private Session session;
    /** FTP 登录用户名 */
    private String username;
    /** FTP 登录密码 */
    private String password;
    /** 私钥 */
    private String privateKey;
    /** FTP 服务器地址IP地址 */
    private String host;
    /** FTP 端口 */
    private int port;

    public Sftp(){}

    /**
     * 构造基于密码认证的sftp对象
     *
     * @param username
     * @param password
     * @param host
     * @param port
     */
    public Sftp(String username, String password, String host, int port) {
        this.username = username;
        this.password = password;
        this.host = host;
        this.port = port;
    }
    /**
     * 连接sftp服务器
     *
     * @throws Exception
     */
    public void login(){
        JSch jsch = new JSch();
        try {
            if (privateKey != null) {
                jsch.addIdentity(privateKey);// 设置私钥
            }
            session = jsch.getSession(username,host,port);

            if(password!=null){
                session.setPassword(password);
            }
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            sftp = (ChannelSftp) channel;
        } catch (JSchException e) {
            e.printStackTrace();
        }
    }
    /**
     * 关闭连接 server
     */
    public void logout() {
        if (sftp != null) {
            if (sftp.isConnected()) {
                sftp.disconnect();
            }
        }
        if (session != null) {
            if (session.isConnected()) {
                session.disconnect();
            }
        }
    }
    /**
     * 将输入流的数据上传到sftp作为文件
     *
     * @param directory    上传到该目录
     * @param sftpFileName sftp端文件名
     * @param input           输入流
     * @throws SftpException
     * @throws Exception
     */
    public void upload(String directory, String sftpFileName, InputStream input) throws SftpException {
        try {
            sftp.cd(directory);
        } catch (SftpException e) {
            sftp.mkdir(directory);
            sftp.cd(directory);
        }
        sftp.put(input, sftpFileName);
    }

    /**
     * 删除sftp作为文件
     *
     * @param directory    文件目录
     * @param sftpFileName sftp端文件名
     * @throws SftpException
     * @throws Exception
     */
    public void delFile(String directory, String sftpFileName)throws SftpException{
        try {
            sftp.cd(directory);
        } catch (SftpException e) {
            sftp.mkdir(directory);
            sftp.cd(directory);
        }
        sftp.rm(sftpFileName);
    }
}
