package com.zhiyuan.orderRepair;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/21
 */
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class OrderRepairMain7050 {
    public static void main(String[] args) {
        SpringApplication.run(OrderRepairMain7050.class,args);
    }
}
