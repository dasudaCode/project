package com.zhiyuan.orderRepair.controller;

import cn.hutool.core.util.IdUtil;
import com.jcraft.jsch.SftpException;
import com.zhiyuan.orderRepair.config.Sftp;
import com.zhiyuan.orderRepair.feign.RepairFeign;
import com.zhiyuan.pojo.Repair;
import com.zhiyuan.utils.CommonResult;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/21
 */
@RestController
public class RepairController {

    private static Properties linuxProperties = new Properties();
    public static String USERNAME;
    public static String PASSWORD;
    public static String IP;
    public static Integer ID;
    public static String UPLOAD_PATH;
    public static String DOWN_PATH;
    public static String PORT;

    static{
        //获取连接linux连接信息
        try {
            linuxProperties.load(RepairController.class.getClassLoader().getResourceAsStream("linux.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        USERNAME = linuxProperties.getProperty("linux.username");
        PASSWORD = linuxProperties.getProperty("linux.password");
        IP = linuxProperties.getProperty("linux.ip");
        ID = Integer.valueOf(linuxProperties.getProperty("linux.id"));
        UPLOAD_PATH = linuxProperties.getProperty("linux.uploadPath");
        DOWN_PATH = linuxProperties.getProperty("linux.downPath");
        PORT = linuxProperties.getProperty("linux.port");

    }

    @Resource
    private RepairFeign repairFeign;

    @PostMapping("/consumer/repair/delRepair")
    public CommonResult<Repair> delRepair(Integer repairId){
        return repairFeign.delRepair(repairId);
    }

    @PostMapping("/consumer/repair/addRepair")
    public CommonResult<Repair> addRepair(Repair repair, MultipartFile file){
        repair.setRepairIdentify("0");
        return repairFeign.insertRepair(repair);
    }

    @PostMapping("/consumer/repair/upload")
    public CommonResult<Map> upload(MultipartFile file){
        String newFileName =null;
        Map<String,Object> map = new HashMap<>();
        if(file != null){
            Sftp sftpUtil = new Sftp(USERNAME,PASSWORD,IP,ID);
            try {
                sftpUtil.login();
                String filename = file.getOriginalFilename();
                if(filename!=null && filename.length() > 0){
                    String s = IdUtil.simpleUUID();
                    newFileName = s+"."+ FilenameUtils.getExtension(filename);
                    sftpUtil.upload(UPLOAD_PATH,newFileName,file.getInputStream());
                    map.put("src",newFileName);
                }
            } catch (SftpException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                //释放连接
                sftpUtil.logout();
            }
        }
        return new CommonResult<>(0,"",map);
    }

    @PostMapping("/consumer/repair/updateRepair")
    public CommonResult<Repair> updateRepair(Repair repair,MultipartFile file){
        System.out.println(repair.getRepairTime());
        return repairFeign.updateRepair(repair);
    }

    @RequestMapping("/consumer/repair/selectRepairByPage")
    public CommonResult<List<Repair>> selectRepairByPage(Integer page,Integer limit,String repairIdentify,Integer plotId,Integer userId){
        Map<String,Object> params = new HashMap<>();
        if(page==null||page<=0){
            page = 1;
        }
        if(limit == null || limit <= 0){
            limit = 10;
        }
        params.put("start",(page-1)*limit);
        params.put("size",limit);
        if(repairIdentify !=null && repairIdentify.length()>0){
            params.put("repairIdentify",repairIdentify);
        }
        if(plotId !=null && plotId>0){
            params.put("plotId",plotId);
        }
        if(userId !=null&& userId > 0){
            params.put("userId",userId);
        }
        return repairFeign.selectRepairByPage(params);
    }
}
