package com.zhiyuan.orderPlot.controller;

import com.zhiyuan.orderPlot.feign.PlotFeign;
import com.zhiyuan.pojo.Plot;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/14
 */
@RestController
public class PlotController {

    @Resource
    private PlotFeign plotFeign;

    @PostMapping("/consumer/plot/addPlot")
    public CommonResult<String> addPlot(Plot plot){
        return plotFeign.addPlot(plot);
    }

    @PostMapping("/consumer/plot/delPlot/{plotId}")
    public CommonResult<String> delPlot(@PathVariable("plotId") Integer plotId){
        return plotFeign.delPlot(plotId);
    }

    @RequestMapping("/consumer/plot/selectPlotByPage")
    public CommonResult<List<Plot>> selectPlotByPage(Integer page,Integer limit,String plotName){
        Map<String,Object> params = new HashMap<>();
        if(page==null||page<=0){
            page = 1;
        }
        if(limit == null || limit <= 0){
            limit = 10;
        }
        params.put("start",(page-1)*limit);
        params.put("size",limit);
        if(plotName!=null&&plotName.length() > 0){
            params.put("plotName","%"+plotName+"%");
        }
        return plotFeign.selectPlotByPage(params);
    }

    @GetMapping("/consumer/plot/getAllPlot")
    public List<Plot> getAllPlot(){
       return plotFeign.getAllPlot();
    }

    @PostMapping("/consumer/plot/updatePlot")
    public CommonResult<String> updatePlot(Plot plot) {
        return plotFeign.updatePlot(plot);
    }
}
