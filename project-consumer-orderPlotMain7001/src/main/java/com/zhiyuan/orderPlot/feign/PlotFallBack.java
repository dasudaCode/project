package com.zhiyuan.orderPlot.feign;

import com.zhiyuan.pojo.Plot;
import com.zhiyuan.remote.PlotRemote;
import com.zhiyuan.utils.CommonResult;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/14
 */
@Component
public class PlotFallBack implements FallbackFactory<PlotRemote> {
    @Override
    public PlotRemote create(Throwable cause) {
        return new PlotRemote(){
            @Override
            public CommonResult<String> addPlot(Plot plot) {
                return new CommonResult<String>(500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<String> updatePlot(Plot plot) {
                return new CommonResult<String>(500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<String> delPlot(Integer plotId) {
                return new CommonResult<String>(500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public CommonResult<List<Plot>> selectPlotByPage(Map<String, Object> params) {
                return new CommonResult<List<Plot>>(500,"服务器繁忙，请稍后再试！");
            }

            @Override
            public List<Plot> getAllPlot() {
                return null;
            }
        };
    }
}
