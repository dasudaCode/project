package com.zhiyuan.orderPlot.feign;

import com.zhiyuan.remote.PlotRemote;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/14
 */
@FeignClient(value = "project-plot-provider",fallbackFactory = PlotFallBack.class)
public interface PlotFeign extends PlotRemote {
}
