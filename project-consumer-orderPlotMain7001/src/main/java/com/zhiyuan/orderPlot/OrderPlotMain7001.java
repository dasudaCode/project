package com.zhiyuan.orderPlot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/14
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class OrderPlotMain7001 {
    public static void main(String[] args) {
        SpringApplication.run(OrderPlotMain7001.class,args);
    }
}
