package com.zhiyuan.plot.controller;

import com.zhiyuan.plot.service.PlotService;
import com.zhiyuan.pojo.Plot;
import com.zhiyuan.pojo.User;
import com.zhiyuan.remote.PlotRemote;
import com.zhiyuan.utils.CommonResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/13
 */
@RestController
@CrossOrigin
public class PlotController implements PlotRemote {

    @Resource
    private PlotService plotService;

    @Override
    public CommonResult<String> addPlot(Plot plot) {
        boolean flag = false;
        flag = plotService.insertPlot(plot);
        if(flag){
            return new CommonResult<String> (200,"success","成功添加了数据");
        }else{
            return new CommonResult<String> (444,"fail","添加失败了");
        }
    }

    @Override
    public CommonResult<String> updatePlot(Plot plot) {
        boolean flag = false;
        flag = plotService.updatePlot(plot);
        if(flag){
            return new CommonResult<String> (200,"success","成功修改了数据");
        }else{
            return new CommonResult<String> (444,"fail","修改失败了");
        }
    }

    @Override
    public CommonResult<String> delPlot(Integer plotId) {
        boolean flag=false;
        List<User> userList = plotService.selectUserByPlotId(plotId);
        if(userList.size() == 0 || userList == null){
           flag =  plotService.deleteByPlotId(plotId);
        }else{
            return new CommonResult<String> (444,"fail","删除失败了,小区还有用户");
        }
        if(flag){
            return new CommonResult<String> (200,"success","成功删除了数据");
        }else{
            return new CommonResult<String> (444,"fail","删除失败了");
        }
    }

    @Override
    public CommonResult<List<Plot>> selectPlotByPage(Map<String, Object> params) {
        int count = plotService.selectPlotCount(params);
        CommonResult<List<Plot>> plotCM = new CommonResult<> ();
        plotCM.setCount(count);
        plotCM.setCode(0);
        plotCM.setData(plotService.selectPlotByPage(params));
        return plotCM;
    }
    @Override
    public List<Plot> getAllPlot() {
        return plotService.selectAllPlot();
    }
}
