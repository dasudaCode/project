package com.zhiyuan.plot.service;

import com.zhiyuan.pojo.Plot;
import com.zhiyuan.pojo.User;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/14
 */
public interface PlotService {
    //分页查询小区
    List<Plot> selectPlotByPage(Map<String,Object> params);

    //分页查询数量
    int selectPlotCount(Map<String,Object> params);

    //通过id查询小区
    Plot selectByPlotId(@RequestParam("PlotId") Integer PlotId);

    //删除小区
    boolean deleteByPlotId(@RequestParam("PlotId") Integer PlotId);

    //添加小区
    boolean insertPlot(Plot plot);

    //更新小区
    boolean updatePlot(Plot plot);

    //查看小区下的用户
    List<User> selectUserByPlotId(@RequestParam("PlotId") Integer PlotId);

    List<Plot> selectAllPlot();
}
