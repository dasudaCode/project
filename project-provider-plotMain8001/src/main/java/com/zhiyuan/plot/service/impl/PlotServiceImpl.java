package com.zhiyuan.plot.service.impl;

import com.zhiyuan.pojo.Plot;
import com.zhiyuan.pojo.User;
import com.zhiyuan.plot.dao.PlotMapper;
import com.zhiyuan.plot.service.PlotService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/14
 */
@Service
public class PlotServiceImpl implements PlotService {

    @Resource
    private PlotMapper plotMapper;
    @Override
    public List<Plot> selectPlotByPage(Map<String, Object> params) {
        return plotMapper.selectPlotByPage(params);
    }

    @Override
    public int selectPlotCount(Map<String, Object> params) {
        return plotMapper.selectPlotCount(params);
    }

    @Override
    public Plot selectByPlotId(Integer PlotId) {
        return plotMapper.selectByPlotId(PlotId);
    }

    @Override
    public boolean deleteByPlotId(Integer PlotId) {
        return plotMapper.deleteByPlotId(PlotId)>=1;
    }

    @Override
    public boolean insertPlot(Plot plot) {
        return plotMapper.insertPlot(plot)>=1;
    }

    @Override
    public boolean updatePlot(Plot plot) {
        return plotMapper.updatePlot(plot)>=1;
    }

    @Override
    public List<User> selectUserByPlotId(Integer PlotId) {
        return plotMapper.selectUserByPlotId(PlotId);
    }

    @Override
    public List<Plot> selectAllPlot() {
        return plotMapper.selectAllPlot();
    }
}
