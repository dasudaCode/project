package com.zhiyuan.plot.dao;

import com.zhiyuan.pojo.Plot;
import com.zhiyuan.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/12
 */
@Mapper
public interface PlotMapper {
    //分页查询小区
    List<Plot> selectPlotByPage(Map<String,Object> params);

    //分页查询数量
    int selectPlotCount(Map<String,Object> params);

    //通过id查询小区
    Plot selectByPlotId(@RequestParam("PlotId") Integer PlotId);

    //删除小区
    int deleteByPlotId(@RequestParam("PlotId") Integer PlotId);

    //添加小区
    int insertPlot( Plot plot);

    //更新小区
    int updatePlot( Plot plot);

    //查看小区下的用户
    List<User> selectUserByPlotId(@RequestParam("PlotId") Integer PlotId);

    List<Plot> selectAllPlot();

}

