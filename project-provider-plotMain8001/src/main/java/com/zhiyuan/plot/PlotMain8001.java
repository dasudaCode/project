package com.zhiyuan.plot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author
 * @remakTodo
 * @Package
 * @date 2020/8/12
 */
@SpringBootApplication
@EnableDiscoveryClient
public class PlotMain8001 {
    public static void main(String[] args) {
        SpringApplication.run(PlotMain8001.class,args);
    }
}
